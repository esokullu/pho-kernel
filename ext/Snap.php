<?php

use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\Content;
use Pho\Kernel\Nodes\Abstracts\ExpirableTrait;


// ephemeral
// time limited database stores.
// works with redis adapter only.

class Snap extends Content {

  use ExpirableTrait;

  const EXPIRE = 60*60*24; // 1 day = 60 (seconds) * 60 (minutes) * 24 (hours)

  public function write(string $content)
  {
    parent::write($content);
    $this->expire(self::EXPIRE);
  }

}
