<?php

use Pho\Kernel\Nodes\Content;

class BlogPost extends Content {

  public function __construct() {
    parent::__construct();
    $this->acl()->chmod(017755); // anyone can read and comment.
    $this->acl()->umask(00022); // friends and other can't edit
  }

  public function setTitle(string $title): void
  {
    $this->setAttribute("title", $title);
  }

  public function getTitle(): string
  {
    return $this->getAttribute("title");
  }

}
