<?php

// every edit is a new database entry with diff
// use git as adapter.

// $this->acl('unix')->chmod(664);

/**
 * Similar to Document in the sense that they're both used for collaboration.
 * But anyone in the context can edit a wiki page.
 * Also a Wiki Page **must** keep a copy of each revision.
 */
class WikiPage extends Document {

    public function __construct() {
        parent::__construct();
        $this->acl()->chmod(007777); // anyone can read and comment.
        $this->acl()->umask(00000); // no limits 
    }

}