<?php

use Pho\Kernel\Nodes\Content;

/**
 * Same as Content, but the content can't exceed 140 characters and we check it
 * in the "write" method.
 *
 * @author Emre Sokullu
 */
class Tweet extends Content {

  const CHARACTER_LIMIT = 140;

  public function __construct() {
    parent::__construct();
    $this->acl()->chmod(017555); // only he can edit, others can read and react
    $this->acl()->umask(00222); // they may choose to change others' ability to read and react
  }

  public function write(string $content)
  {
    if(strlen($content)>CHARACTER_LIMIT) {
      throw \InvalidArgumentException("A tweet cannot contain more than %s characters.", (string) CHARACTER_LIMIT);
    }
    parent::write($content);
  }

}
