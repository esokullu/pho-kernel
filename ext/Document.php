<?php

use Pho\Kernel\Nodes\Content;

/**
 * Documents to collaborate on.
 * Structurally similar to a blog post which comes with a title.
 * The difference is in permissions. Blog Post has sticky bit on, 
 * Document doesn't because editors can invite others to edit as well.
 */
class Document extends BlogPost {

    public function __construct() {
        parent::__construct();
        $this->acl()->chmod(007755); // no sticky bit. anyone can read and comment.
    }

}