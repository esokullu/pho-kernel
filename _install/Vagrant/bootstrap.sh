#!/usr/bin/env bash

## Variables
PROJECT_FOLDER="/usr/share/pho-kernel"
REDIS_URL="https://github.com/antirez/redis/archive/4.0-rc2.tar.gz"
REDIS_MODULE_GRAPH_GIT_REPO="https://github.com/swilly22/redis-module-graph.git"
## General apt-get update & upgrade
sudo apt-get update
sudo apt-get -y upgrade

## Install redis
sudo apt-get -y install redis-server

## Install PHP **7.1**
# https://ayesh.me/Ubuntu-PHP-7.1
# Required extensions: apcu
sudo add-apt-repository -y ppa:ondrej/php
sudo apt-get update
sudo apt-get install -y php7.1 php7.1-common php7.1-apcu php7.1-curl php7.1-xml php7.1-zip php7.1-gd php7.1-mbstring

## Install PHP composer
sudo apt-get install -y composer

## Install requirements
cd {$PROJECT_FOLDER}
composer install

## Install Redis (4.x)
cd /tmp
wget -qO redis.tar.gz ${REDIS_URL}
tar zxvf redis.tar.gz
rm redis.tar.gz
cd redis*
make && make install
make clean
CDIR=`pwd`
cd ..
rm -fr ${CDIR}

## Install redis-module-graph
cd /tmp
git clone ${REDIS_MODULE_GRAPH_GIT_REPO}
cd redis-module-graph/
cmake .
make module
