##Kernel

* kernel.booted_up
* kernel.configured|$configs
* kernel.halted

#Edges
* edge.created|[$edge_id, $subject, $object, $predicate_label]
* edge.set_for_expiration|[$subject, $object, $predicate_label,$timeout]

#Nodes
* node.created|$id, $label
* node.modified|$id, $label, $attribute
* node.set_for_expiration | $id, $seconds

#Users
* user.followed|$follower,$followed
// * content.posted|$poster_id, $content_id
// * user.reacted|$poster_id, $content_id
* group.created|$creator, $group_id
* group.joined|$user_id, $group_id
* group.left|$user_id, $group_id

# Objects
* object.posted|$poster_id, $content_id
* content.posted|$poster_id, $content_id
* object.drafted|$poster_id, $content_id

! bunun icin bir class olustur, ve bu check edilsin, bu mu emit ediliyor diye.
