```
$user  = new User();
$content = new Content();
$user->postTo($content, $network);
$user->postTo($content->write("Hello world!"), $network);

////
$user1 = new User();
$content = $user1->write("Hello world!") /* Draft */ ->post($network) /* Content (but also Revision in the bg) */;

$user2 = new User();
$user2->write("Welcome...") /* Draft */ ->react($content) /* Reaction */;

$user1->write("Second revision")->edit($content);

// Draft -> Revision -> Content
// Content->showRevisions();
// Revision->diff(Revision) => ...

```

Decorator ile Content
content 
Expirable

REQUIREMENTS
----
PHP 7.1
Redis 3.9+

TODOs
---
* Clean up phpunit
* Refactor edgesfrom -> edgesout, edgesto ->edgesin
* Refactor unused code
* Refactor exceptions
* No services yet.
* EdgeFactory ve NodeFactory icin runtime cache mekanizmasi
* implement Ban as Edge.

pho-cli
pho-kernel
pho-shell

pho-cli
pho list (lists kernels)
pho draft (creates a new directory where you can play with configs)
pho test (--with-graphql --with-rest (default) --with-gremlin --with-cypher --with-cql)
pho new (starts new kernel in vagrant if no config)
pho attach (starts a ssh session)
pho shell (starts a shell session)
pho cluster (follows a replica)
pho production (transfers vagrant to docker)

pho-shell => also vfs
ls (shows nodes)
mkdir (creates a group)
touch


Pho Community Edition
easy to use based on vagrant and docker

Pho Cloud (later...)
8/month
starts on Netherlands
then you can transfer community edition to your host of choice such as AWS or digitalocean

Pho Enterprise
custom installation for maximum performance
guarantee sla


--

anonymous user