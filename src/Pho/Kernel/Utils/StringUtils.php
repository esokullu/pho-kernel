<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Utils;

use Pho\Kernel\Kernel;

class StringUtils {

  /**
   * Strips off the namespace from the full class name, leaving the
   * classname itself only.
   *
   * @param string  $class_name Full class name including its namespace.
   *
   * @return string Trimmed class name.
   */
  public static function trimNamespace(string $class_name): string
  {
    if ($pos = strrpos($class_name, '\\'))
      return substr($class_name, $pos + 1);
    return $class_name;
  }

  public static function buildKey(string $node_id, string $edge_class): string
  {
    return $node_id . Kernel::config()->database_key_separator . self::trimNamespace($edge_class);
  }

  public static function keyToPredicate(string $key): string
  {
      return Kernel::config()->namespaces->predicates . $key. "Predicate";
  }

  public static function edgeToKey(string $edge_class): string
  {
    return substr(self::trimNamespace($edge_class),0, -1 * strlen("Edge"));
  }

  public static function edgeToPredicate(string $edge_class): string
  {
    return self::keyToPredicate(self::edgeToKey($edge_class));
  }

  /**
   * Generates a cryptographically secure random UUID(v4) for internal use.
   *
   * @link  https://en.wikipedia.org/wiki/Universally_unique_identifier UUIDv4 format
   *
   * @return  String  random uuid in guid v4 format
   */
  public static function genUUID(): string
  {
    $data = random_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
  }


  /**
	 * Returns true if $haystack starts with $needle (case-sensitive)
	 *
	 * For example:
	 *
	 *     Str::startsWith('foobar', 'bar');  // returns false
	 *     Str::startsWith('foobar', 'foo');  // returns true
	 *     Str::startsWith('foobar', 'FOO');  // returns false
	 *     Str::startsWith('foobar', '');     // returns false
	 *     Str::startsWith('', 'foobar');     // returns false
	 *
	 * @since  0.1.0
	 *
	 * @param  string  $haystack  the string to search
	 * @param  string  $needle    the substring to search for
	 *
	 * @return  bool  true if $haystack starts with $needle
	 *
	 * @throws  \BadMethodCallException    if $haystack or $needle is omitted
	 * @throws  \InvalidArgumentException  if $haystack is not a string
	 * @throws  \InvalidArgumentException  if $needle is not a string
	 *
	 * @see  \Jstewmc\PhpHelpers\Str::startsWith()  case-insensitive version
	 * @link  http://stackoverflow.com/a/834355  MrHus' answer to "startsWith() and
	 *    endsWith() functions in PHP" on StackOverflow
	 */
	public static function startsWith($haystack, $needle)
	{
		$startsWith = false;

		// if $haystack and $needle are given
		if ($haystack !== null && $needle !== null) {
			// if $haystack is a string
			if (is_string($haystack)) {
				// if $needle is a string
				if (is_string($needle)) {
					// if $haystack is not empty
					if (strlen($haystack) > 0) {
						// if $needle is not empty
						if (strlen($needle) > 0) {
							$startsWith = ! strncmp($haystack, $needle, strlen($needle));
						} else {
							$startsWith = false;
						}
					} else {
						$startsWith = false;
					}
				} else {
					throw new \InvalidArgumentException(
						__METHOD__." expects the second parameter, the needle, to be a string"
					);
				}
			} else {
				throw new \InvalidArgumentException(
					__METHOD__." expects the first parameter, the haystack, to be a string"
				);
			}
		} else {
			throw new \BadMethodCallException(
				__METHOD__." expects two string parameters, haystack and needle"
			);
		}

		return $startsWith;
	}


  /**
 * Returns true if $haystack starts with $needle (case-insensitive)
 *
 * For example:
 *
 *     Str::iStartsWith('foobar', 'bar');  // returns false
 *     Str::iStartsWith('foobar', 'foo');  // returns true
 *     Str::iStartsWith('foobar', 'FOO');  // returns true
 *     Str::iStartsWith('', 'foobar');     // returns false
 *     Str::iStartsWith('foobar', '');     // returns false
 *
 * @since  0.1.0
 *
 * @param  string  $haystack  the case-insensitive string to search
 * @param  string  $needle    the case-insensitive substring to search for
 *
 * @return  bool  true if $haystack ends with $needle
 *
 * @throws  \BadMethodCallException    if $haystack or $needle is omitted
 * @throws  \InvalidArgumentException  if $haystack is not a string
 * @throws  \InvalidArgumentException  if $needle is not a string
 *
 * @see     \Jstewmc\PhpHelpers\Str::startsWith()  case-sensitive version
 */
public static function iStartsWith($haystack, $needle)
{
  $startsWith = null;

  // if $haystack and $needle are given
  if ($haystack !== null && $needle !== null) {
    // if $haystack is a string
    if (is_string($haystack)) {
      // if $needle is a string
      if (is_string($needle)) {
        $startsWith = self::startsWith(strtolower($haystack), strtolower($needle));
      } else {
        throw new \InvalidArgumentException(
          __METHOD__."() expects parameter two, needle, to be a string"
        );
      }
    } else {
      throw new \InvalidArgumentException(
        __METHOD__."() expects parameter one, haystack, to be a string"
      );
    }
  } else {
    throw new \BadMethodCallException(
      __METHOD__."() expects two string parameters, haystack and needle"
    );
  }

  return $startsWith;
}

/**
	 * Returns true if $haystack ends with $needle (case-sensitive)
	 *
	 * For example:
	 *
	 *     Str::endsWith('foobar', 'bar');  // returns true
	 *     Str::endsWith('foobar', 'baz');  // returns false
	 *     Str::endsWith('foobar', 'BAR');  // returns false
	 *     Str::endsWith('foobar', '');     // returns false
	 *     Str::endsWith('', 'foobar');     // returns false
	 *
	 * @since  0.1.0
	 *
	 * @param  string  $haystack  the string to search
	 * @param  string  $needle    the substring to search for
	 *
	 * @return  bool  true if $haystack ends with $needle
	 *
	 * @throws  \BadMethodCallException    if $haystack or $needle is omitted
	 * @throws  \InvalidArgumentException  if $haystack is not a string
	 * @throws  \InvalidArgumentException  if $needle is not a string
	 *
	 * @see  \Jstewmc\PhpHelpers\Str::iEndsWith()  case-insensitive version
	 * @link  http://stackoverflow.com/a/834355  MrHus' answer to "startsWith()
	 *    and endsWith() functions in PHP" on StackOverflow
	 */
	public static function endsWith($haystack, $needle)
	{
		$endsWith = false;

		// if $haystack and $needle are passed
		if ($haystack !== null && $needle !== null) {
			// if $haystack is a string
			if (is_string($haystack)) {
				// if $needle is a string
				if (is_string($needle)) {
					// if $haystack is not an empty string
					if (strlen($haystack) > 0) {
						// if $needle is not an empty string
						if (strlen($needle) > 0) {
							$endsWith = substr($haystack, -strlen($needle)) === $needle;
						} else {
							$endsWith = false;
						}
					} else {
						$endsWith = false;
					}
				} else {
					throw new \InvalidArgumentException(
						__METHOD__." expects the second parameter, the needle, to be a string"
					);
				}
			} else {
				throw new \InvalidArgumentException(
					__METHOD__." expects the first parameter, the haystack, to be a string"
				);
			}
		} else {
			throw new \BadMethodCallException(
				__METHOD__." expects two string parameters"
			);
		}

		return $endsWith;
	}

	/**
	 * Returns true if $haystack ends with $needle (case-insensitive)
	 *
	 *     Str::endsWith('foobar', 'bar');  // returns true
	 *     Str::endsWith('foobar', 'baz');  // returns false
	 *     Str::endsWith('foobar', 'BAR');  // returns true
	 *
	 * @since  0.1.0
	 *
	 * @param  string  $haystack  str  the string to search
	 * @param  string  $needle    str  the substring to search for
	 *
	 * @return  bool
	 *
	 * @throws  \BadMethodCallException    if $haystack or $needle is omitted
	 * @throws  \InvalidArgumentException  if $haystack is not a string
	 * @throws  \InvalidArgumentException  if $needle is not a string
	 *
	 * @see  \Jstewmc\PhpHelpers\Str::iEndsWith()  case-sensitive version
	 */
	public static function iEndsWith($haystack, $needle)
	{
		$endsWith = null;

		// if $haystack and $needle are given
		if ($haystack !== null && $needle !== null) {
			// if $haystack is a string
			if (is_string($haystack)) {
				// if $needle is a string
				if (is_string($needle)) {
					$endsWith = self::endsWith(strtolower($haystack), strtolower($needle));
				} else {
					throw new \InvalidArgumentException(
						__METHOD__."() expects parameter two, needle, to be a string"
					);
				}
			} else {
				throw new \InvalidArgumentException(
					__METHOD__."() expects parameter one, haystack, to be a string"
				);
			}
		} else {
			throw new \BadMethodCallException(
				__METHOD__."() expects two string parameters, haystack and needle"
			);
		}

		return $endsWith;
	}

}
