<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Utils;

/**
 * Object-oriented Utils
 */
class OoUtils {

  /**
   * Returns a Generator (aka a traversable object for loops) with the ancestors
   * of the given class.
   *
   * @param string $class Top class name.
   *
   * @return \Generator Ancestors.
   */
  public static function getAncestors (string $class, bool $include_top = true): \Generator
  {
    if($include_top)
      yield $class;
    for (; $class = get_parent_class ($class); yield $class);
  }

}
