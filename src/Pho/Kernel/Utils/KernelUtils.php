<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

 namespace Pho\Kernel\Utils;

 use Pho\Kernel\Kernel;
 use Pho\Kernel\Services\{ServiceInterface, ServiceFactory};
 use Pho\Kernel\Nodes\NodeFactory;
 use Pho\Kernel\Nodes\Exceptions\NodeDoesNotExistException;
 use Pho\Kernel\Nodes\Abstracts\AbstractGraph;
 use Zend\Config\Config;

/**
 * This class contains utility functions
 * that will only be used by the Kernel class.
 *
 * @author Emre Sokullu
 */
 class KernelUtils {

   /**
    * @var Zend\Config\Config
    */
   protected static $config;

   /**
    * @var bool
    */
   protected static $is_configured = false;

   /**
    * @var array contains variables of type Pho\Kernel\Service\ServiceInterface
    */
   protected static $services;

   /**
    * @var boolean
    */
   protected static $is_running = false;

   /**
    * @var Pho\Kernel\Nodes\Abstracts\AbstractGraph
    */
    protected $graph;


   protected function registerListeners(AbstractGraph $graph): void
   {
     $this->service("logger")->info(sprintf(
       "Registering listeners."
     ));

     $nodes = $graph->getContainees();
     array_unshift($nodes, $graph);
     $node_count = count($nodes);

     $this->service("logger")->info(sprintf(
       "Total # of nodes for the graph \"%s\": %s", $graph->getId(), (string) $node_count
     ));

     for($i=0; $i<$node_count; $i++) {

       $node = $nodes[$i];

       $this->service("logger")->info(sprintf(
         "Registering listeners for node %s, a %s", $node->getId(), $node->getLabel()
       ));

       $ref = new \ReflectionObject($node);
       $ref_methods = $ref->getMethods( \ReflectionMethod::IS_PUBLIC );

       $this->service("logger")->info(sprintf(
         "Node methods are: %s", print_r($ref_methods, true)
       ));

       array_walk($ref_methods, function($item, $key) use ($node) {
         if(preg_match("/^handle([A-Z][a-z]+)([A-Z][a-z]+)$/",$item->name,$item_parts)) {

           $this->service("logger")->info(sprintf(
             "Adding a listener on %s with id %s", $node->getLabel(), $node->getId()
           ));

           $this->service("events")->on(
            strtolower($item_parts[1].".".$item_parts[2]), [$node, $item->name]
          );
         }
       });

       // recursiveness
       if($node instanceof AbstractGraph and $node->getId()!=$graph->getId()) {
         $this->registerListeners($node);
       }

       // memory management.
       // clean up after use.
       unset($nodes[$i]); // $nodes[$i] = null;
       // gc_collect_cycles(); // perhaps give the user option to choose Fast Boot vs Memory Controlled Boot (good for resource-constrainted distributed systems that may tolerate slow boot)

     }
   }

   /**
    * Starts all kernel services in a single shot.
    */
   protected function startServices(): void
   {
     $this->startService("database");
     $this->startService("index");
     $this->startService("logger");
     $this->startService("storage");
     $this->startService("events");
   }

   /**
    * Ensures that there is a root Graph attached to the kernel.
    * Used privately by the kernel.
    */
   protected function seedRoot(): void
   {
     try {
       $this->graph = NodeFactory::reconstruct($this->config()->root_graph_key);
     } catch(NodeDoesNotExistException $e) {
       $this->graph =  NodeFactory::build($this->config()->root_graph_label);
     }
   }

   protected static function runs(): void
   {
     self::$is_running = true;
   }

   protected static function isRunning(): bool
   {
     return (boolean) self::$is_running;
   }

   protected function startService(string $service): void
   {
     self::$services[$service]  = ServiceFactory::create($service, $this->config()->services->{$service}->type, $this->config()->services->{$service}->uri);
   }




 }
