<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel;

use Pho\Kernel\Utils\KernelUtils;
use Pho\Kernel\Nodes\NodeFactory;
use Pho\Kernel\Nodes\Abstracts\AbstractGraph;
use Pho\Kernel\Exceptions\{KernelIsAlreadyRunningException, InvalidEntityException, KernelNotRunningException, ServiceUnavailableException};
use Pho\Kernel\Services\ServiceInterface;

use Zend\Config\Config;

/**
 * Pho Kernel is a programmable interface as well as a configuration
 * and events broker for the distributed micro social graph.
 *
 * Pho Kernel is static, therefore you cannot run multiple kernels in a single
 * PHP thread. Yo may however halt the kernel (via halt() method) and relaunch.
 *
 * Example usage:
 *  $kernel = new Pho\Kernel\Kernel();
 *  $kernel->boot();
 *  $network = $kernel->graph();
 *  print_r($network->getContainees());
 *
 * @author Emre Sokullu
 */
class Kernel extends KernelUtils implements KernelInterface {

  /**
   * @var Pho\Kernel\Events
   */
  private static $EventEmitter;


  /**
   * Constructor.
   *
   * @param $configs  array  service configurations
   */
  public function __construct( ?array $configs = null )
  {
    if(self::isRunning()) {
      throw new KernelIsAlreadyRunningException("You cannot create multiple kernels in a single thread.");
    }
    //self::$acl = new AclDirector();
    $this->reconfigure( $configs );
  }

  /**
   * {@inheritdoc}
   */
  public static function events(): ServiceInterface
  {
    return self::service("events");
  }


  

  public function halt(): void
  {
      if(!self::isRunning()) {
        throw new KernelNotRunningException("You must boot up the kernel before requesting the root graph.");
      }
      self::events()->emit("kernel.halted");
      self::$EventEmitter = null;
      self::$config = null;
      self::$is_configured  = false;
      unset($this->graph);
      self::$services = [];
      self::$is_running = false;
  }


  /**
   * {@inheritdoc}
   */
  public static function reconfigure( ?array $configs=null ): void
  {
    if(self::isRunning()) {
      throw new KernelIsAlreadyRunningException("You cannot reconfigure a running kernel.");
    }

    self::$config = new Config(include __DIR__ . DIRECTORY_SEPARATOR . "Defaults.php");

    if(!is_null($configs)) {
      self::$config->merge(new Config($configs));
    }

    self::$is_configured = true;

  }

  /**
   * {@inheritdoc}
   */
   public static function config(): Config
   {
    if(!self::$is_configured) {
      self::reconfigure();
    }
    return self::$config;
   }

  /**
   * {@inheritdoc}
   */
  public function boot(): void
  {
    if(self::isRunning()) {
      throw new KernelIsAlreadyRunningException("You cannot reconfigure a running kernel.");
    }
    self::runs();
    $this->startServices();
    $this->seedRoot();
    $this->registerListeners($this->graph);

    self::events()->emit("kernel.booted_up");
  }

  /**
   * {@inheritdoc}
   */
  public function graph(): AbstractGraph
  {
    if(!self::isRunning()) {
      throw new KernelNotRunningException("You must boot up the kernel before requesting the root graph.");
    }
    return $this->graph; // NodeFactory::create(self::$services["config"]->root_graph_key);
  }

  /**
   * {@inheritdoc}
   */
  public static function service(string $service_type): ServiceInterface
  {
    if(!self::isRunning()) {
      throw new KernelNotRunningException(sprintf("You cannot access services while the kernel is off. Attempted with service: %s", $service_type));
    }
    if(!isset(self::$services[$service_type])) {
      throw new ServiceUnavailableException(sprintf("The service %s does not exist.", $service_type));
    }
    return self::$services[$service_type];
  }

  /**
   * @todo think
   */
  public function registerEntity(): void
  {
    // if(class_implements && class_parents)
  }

  public function registerAdapter(): void
  {

  }

  public function listNodes(): array
  {

  }

  public function index(): void
  {

  }

  public static function isToIndex(string $node_label, string $attribute): bool
  {
    return false;
  }



}
