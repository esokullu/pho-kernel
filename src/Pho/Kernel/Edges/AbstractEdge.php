<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges;

use Pho\Kernel\Kernel;
use Pho\Kernel\Utils\{StringUtils, OoUtils};
use Pho\Kernel\Edges\Exceptions\PredicateNotSetProperlyException;
use Pho\Kernel\Predicates\PredicateInterface;
use Pho\Kernel\Nodes\Abstracts\NodeInterface;
use Pho\Kernel\Nodes\NodeFactory;

abstract class AbstractEdge /* MUST NOT extend any other class!! */ implements EdgeInterface, \Serializable {

  use EdgeUtilsTrait;

  /**
   * @var NodeInterface
   */
  protected $id;

  /**
   * @var NodeInterface
   */
  protected $from;

  /**
   * @var NodeInterface
   */
  protected $to;

  /**
   * @var PredicateInterface
   */
  protected $predicate;

  /**
   * @var string
   */
  protected $label;

  /**
   * @var int Timestamp.
   */
  protected $created_at;

  /**
   * @var array
   */
  protected $attributes;

  /**
   * @var string // we keep it as string because we don't want recursive
   */
  protected $reciprocal;

  /**
   * @var bool
   */
   protected $is_orphan = true;

  public function __construct(NodeInterface $source, NodeInterface $target)
  {
    $this->label = StringUtils::trimNamespace(get_class($this));
    $this->predicate = $this->generatePredicate($this->label);
    if( !isset($this->predicate) || !$this->predicate instanceof PredicateInterface) {
      throw new PredicateNotSetProperlyException();
    }
    $this->id = StringUtils::genUUID();
    $this->from = $source;
    $this->to = $target;
    $this->created_at = time();
  }

  public function setReciprocal(EdgeInterface $edge): EdgeInterface
  {
    if(isset($this->reciprocal)) {
      throw new ReciprocalAlreadySetException("Reciprocal already set.");
    }
    // undirectededgeexception ?
    $this->reciprocal = $edge->getId();
    return $this;
  }

  public function getReciprocal(): EdgeInterface
  {
    return EdgeFactory::reconstruct($this->reciprocal);
  }

  public function __toString(): string
  {
    return $this->id;
  }

  public function save(): void
  {
    $this->connect();
    Kernel::service("database")->set($this->getId(), serialize($this));
  }

  public function connect(): void
  {
    foreach(OoUtils::getAncestors(get_class($this)) as $key) {
      Kernel::service("database")->list_add(
        StringUtils::buildKey($this->from->getId(), $key),
        $this->getId()
      );
    }
    $this->is_orphan = false;
    Kernel::events()->emit("edge.created", [$this->getId(), $this->from->getId(), $this->to->getId(), $this->getPredicate()->getLabel()]);
  }

  public function getLabel(): string
  {
      return $this->label;
  }

  public function setLabel(string $label): void
  {
    $this->label = $label;
  }



  public function serialize(): string
  {
    return serialize([
      "id" => $this->id,
      "label" => $this->label,
      "predicate" => $this->predicate->getLabel(),
      "from" => $this->from->getId(),
      "to" => $this->to->getId(),
      "reciprocal" => $this->reciprocal,
      "created_at" => $this->created_at,
      "is_orphan" => $this->is_orphan,
      "attributes" => $this->attributes
    ]);
  }

  public function unserialize($data): void
  {
    $data = unserialize($data);
    $this->id = $data["id"];
    $this->label = $data["label"];
    $this->predicate = $this->generatePredicate($data["predicate"]);
    $this->from = NodeFactory::reconstruct($data["from"]);
    $this->to =  NodeFactory::reconstruct($data["to"]);
    $this->reciprocal = $data["reciprocal"];
    $this->created_at = $data["created_at"];
    $this->is_orphan = $data["is_orphan"];
    $this->attributes = $data["attributes"];
  }

  public function isOrphan(): bool
  {
    return $this->is_orphan;
  }

  public function getId(): string
  {
    return $this->id;
  }

  public function isDirected(): bool
  {
    return $this->predicate->hasReciprocal();
  }

  public function getFrom(): NodeInterface
  {
    return $this->from;
  }

  public function getTo(): NodeInterface
  {
    return $this->to;
  }

  public function getPredicate(): PredicateInterface
  {
    return $this->predicate;
  }

  public function destroy(bool $recursive = true): void
  {
    if($recursive) {
      $this->getReciprocal()->destroy(false);
    }
    Kernel::service("database")->delete($this->getId());
    $this->disconnect();
    Kernel::events()->emit("edge.deleted", [$this->getId(), $this->getPredicate()->getLabel()]);
  }

  public function expire(int $timeout, bool $recursive = true): void
  {
    if($recursive) {
      $this->getReciprocal()->expire($timeout, false);
    }
    Kernel::service("database")->expire($this->getId(), $timeout);
    /*Kernel::service("database")->expire(
      StringUtils::buildKey($this->from->getId(), $this->getPredicate()->getLabel())
      , $timeout
    );*/
    $this->expireAncestors($timeout);
    Kernel::events()->emit("edge.set_for_expiration", [$this->getId(), $this->getPredicate()->getLabel(), $timeout]);
  }

  public function getTTL(): int
  {
    return Kernel::service("database")->getTTL($this->getId());
  }

}
