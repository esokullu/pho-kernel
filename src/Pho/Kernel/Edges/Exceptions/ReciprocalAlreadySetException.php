<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges\Exceptions;

/**
 * Thrown when there is an attempt to change the reciprocal of the edge.
 *
 * @author Emre Sokullu
 */
class ReciprocalAlreadySetException extends \Exception {

}
