<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges\Exceptions;

/**
 * Thrown when the given edge ID does not exist in the database.
 *
 * @author Emre Sokullu
 */
class EdgeDoesNotExistException extends \Exception {

}
