<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges\Exceptions;

/**
 * Thrown when the factory has attempted to build an edge but the ID does not
 * belong to one, but some other entity. This exception may be merged
 * with EdgeDoesNotExistException in the future, currently it extends it.
 *
 * @author Emre Sokullu
 */
class NotAnEdgeException extends EdgeDoesNotExistException /* because they are practically similar, also think about exception chaining. */
{

}
