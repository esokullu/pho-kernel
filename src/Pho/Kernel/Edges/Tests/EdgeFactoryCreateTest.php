<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges\Tests;

use Pho\Kernel\Kernel;
use Pho\Kernel\Tests\TestCase;
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Predicates\CorePredicates;
use Pho\Kernel\Nodes\Group;

class EdgeFactoryCreateTest extends TestCase {
  private $edges;
  private $group1, $group2;

  public function setUp() {
    parent::setUp();
    
    $this->group1 = new Group();
    $this->group2 = new Group();
    $this->edges = EdgeFactory::build($this->group1, $this->group2, CorePredicates::CONTAINS);

    $this->setupRedis();
  }

  public function tearDown() {
    unset($this->edges);
    unset($this->group1);
    unset($this->group2);
    parent::tearDown();
  }

  public function testCreate() {
    $expected = CorePredicates::CONTAINS;
    $this->assertEquals($expected, $this->edges[0]->getPredicate()->getLabel()); // or assertRegExp
    $expected = $this->group1->getId();
    $this->assertEquals($expected, $this->edges[0]->getFrom()->getId());
    $expected = $this->group2->getId();
    $this->assertEquals($expected, $this->edges[0]->getTo()->getId());
    $expected = CorePredicates::BELONGS;
    $this->assertEquals($expected, $this->edges[1]->getPredicate()->getLabel()); // or assertRegExp
    $expected = $this->group2->getId();
    $this->assertEquals($expected, $this->edges[1]->getFrom()->getId());
    $expected = $this->group1->getId();
    $this->assertEquals($expected, $this->edges[1]->getTo()->getId());
    if(!is_null($this->redis)) {
      $group1_edges = $this->redis->keys($this->group1->getId()."/*");
      $this->assertCount(5, $group1_edges, print_r($group1_edges, true)); // includes abstractedge, contains and belongs (also pub/sub extended)
      $group2_edges = $this->redis->keys($this->group2->getId()."/*");
      $this->assertCount(3, $group2_edges, print_r($group2_edges, true)); // includes abstractedge and belongs only.  (also pub/sub extended)
    }
  }

}
