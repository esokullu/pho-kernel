<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges\Tests;

use Pho\Kernel\Kernel;
use Pho\Kernel\Tests\TestCase;
use Pho\Kernel\Edges\Exceptions\EdgeDoesNotExistException;
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Predicates\CorePredicates;
use Pho\Kernel\Nodes\Group;

class EdgeObjectTest  extends TestCase {
  private $edges;
  private $group1, $group2;
  private $edge0_id, $edge1_id;

  public function setUp() {
    $this->startKernel();

    $this->group1 = new Group();
    $this->group2 = new Group();
    $this->edges = EdgeFactory::build($this->group1, $this->group2, CorePredicates::CONTAINS);

    $this->edge0_id = $this->edges[0]->getId();
    $this->edge1_id = $this->edges[1]->getId();

    $this->kernel->service("logger")->info(
      sprintf("%s test class has created the following nodes: %s, %s and edges: %s, %s", __CLASS__, $this->group1->getId(), $this->group2->getId(), $this->edge0_id, $this->edge1_id)
    );
  }

  public function tearDown() {
    unset($this->edges);

    // order is important, first 2 then 1
    $this->group2->destroy();
    $this->group1->destroy();

    unset($this->group1);
    unset($this->group2);
    $this->stopKernel();
  }

  /**
   * @expectedException Pho\Kernel\Edges\Exceptions\EdgeDoesNotExistException
   */
  public function testDestroy0() {
    $this->edges[0]->destroy();
    $this->expectException(EdgeFactory::reconstruct($this->edge0_id));
  }

  /**
   * @expectedException Pho\Kernel\Edges\Exceptions\EdgeDoesNotExistException
   */
  public function testDestroy1() {
    // because the previous one was reciprocal
    $this->edges[0]->destroy();
    $this->expectException(EdgeDoesNotExistException::class);
    EdgeFactory::reconstruct($this->edge1_id);
  }

}
