<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges\Tests;

use Pho\Kernel\Kernel;
use Pho\Kernel\Tests\TestCase;
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Predicates\CorePredicates;
use Pho\Kernel\Nodes\Group;
use Pho\Kernel\Edges\Exceptions\EdgeAlreadyExistsException;

class EdgeFactoryReconstructTest extends TestCase {

  private $edges;
  private $group;

  public function setUp() {
    parent::setUp();
    $this->group = new Group();
    $this->edges = EdgeFactory::reconstructAllInArray($this->group);

    $this->setupRedis();
  }

  public function tearDown() {
    unset($this->edges);
    unset($this->group);
    parent::tearDown();
  }

  /**
   * @expectedException Pho\Kernel\Edges\Exceptions\EdgeAlreadyExistsException
   */
  public function testDuplicateReconstruct() {
    // because this edge was already created through signals and slots.
    $this->kernel->graph()->contains($this->group);
    return;
    try {
      $this->kernel->graph()->contains($this->group);
    }
    catch(\Exception $e) {
      $this->expectException(get_class($e), "it was: ".get_class($e));
    }
  }

  public function testReconstruct() {
    $expected = CorePredicates::BELONGS;
    $this->assertEquals($expected, $this->edges[0]->getPredicate()->getLabel(), print_r($this->edges, true)); // or assertRegExp
    $expected = 1;
    $this->assertCount($expected, $this->edges);

    /*$expected = CorePredicates::CONTAINS;
    $this->assertEquals($expected, $this->edges[0]->getReciprocal()->getLabel()); // or assertRegExp*/
    if(!is_null($this->redis)) {
      $group1_edges = $this->redis->keys($this->group->getId()."/*");
      $this->assertCount(3, $group1_edges, print_r($group1_edges, true)); // includes abstractedge  (also pub/sub extended)
    }
  }

}
