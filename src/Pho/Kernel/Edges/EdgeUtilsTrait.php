<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges;

use Pho\Kernel\Kernel;
use Pho\Kernel\Predicates\{PredicateInterface, AbstractPredicate};
use Pho\Kernel\Utils\{StringUtils, OoUtils};

trait EdgeUtilsTrait {
  protected function generatePredicate(string $label): PredicateInterface
  {
      return new class($label) extends AbstractPredicate {};
  }

  // "e110c6a0-fb6b-42a7-a017-8e891d11b934/Belongs" => []
  protected function disconnect(): void
  {
    foreach(OoUtils::getAncestors(get_class($this)) as $key) {
      Kernel::service("database")->list_delete(
        StringUtils::buildKey($this->from->getId(), $key), $this->getId()
      );
    }
  }


  // YES -- was problematic
  // ok to expire Contains and Creates
  // but not AbstractEdge
  // maybe problematic??
  // because expiring the whole list. not the entry only.
  protected function expireAncestors(int $timeout): void
  {
    //return;
    foreach(OoUtils::getAncestors(get_class($this)) as $key) {
      Kernel::service("database")->expire(
        StringUtils::buildKey($this->from->getId(), $key)
        , $timeout
      );
    }
  }
  
}
