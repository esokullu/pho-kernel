<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges;

use Pho\Kernel\{Kernel};
use Pho\Kernel\Utils\StringUtils;
use Pho\Kernel\Nodes\Abstracts\NodeInterface;
use Pho\Kernel\Edges\{EdgeInterface, EdgeFactoryUtils, EdgeUtilsTrait};
use Pho\Kernel\Edges\Exceptions\{NotAnEdgeException, EdgeDoesNotExistException, EdgeAlreadyExistsException};

class EdgeFactory extends EdgeFactoryUtils {



  /**
   * Creates an edge with given subject, object and predicate, including their
   * reciprocal if it's available and returns the objects in an array.
   *
   * @param NodeInterface $subject Source node.
   * @param NodeInterface $object Target node.
   * @param string  $predicate Predicate label.
   *
   * @return array The newly created edge objects. Only one if no reciprocal available.
   */
  public static function build(NodeInterface $subject, NodeInterface $object, string $predicate): array
  {
    if(self::edgeExists( $subject,  $object,  $predicate)) {
      throw new EdgeAlreadyExistsException(
        sprintf("An edge with the label %s between nodes %s and %s exists.", $predicate, $subject->getId(), $object->getId())
      );
    }
    
    //if(self::aclBroker()->isPermissible($subject, $object, $predicate)) {

    //}

    $return = [];
    $edge_class = self::predicateUtils()->toEdge($predicate);
    $return[] = new $edge_class($subject, $object);

    if(isset($return[0]) && $return[0] instanceof EdgeInterface && $return[0]->getPredicate()->hasReciprocal()) {
      $edge_class = $return[0]->getPredicate()->getReciprocal()->getEdgeClass();
      $return[] = new $edge_class($object, $subject);
      $return[0]->setReciprocal($return[1])->save();
      $return[1]->setReciprocal($return[0])->save();
    }
    return $return;
  }



  public static function destroy(NodeInterface $subject, NodeInterface $object, string $predicate): void
  {
    $edge = $this->getEdge($subject, $object, $predicate);
    $edge->disconnect();
  }


  /**
   * Reconstructs an edge object based on its ID.
   *
   * @param string  $id
   *
   * @return  EdgeInterface   The edge object.
   *
   * @throws  EdgeDoesNotExistException when the given id does not exist in the database.
   * @throws NotAnEdgeException when the given id does not belong to an edge.
   */
  public static function reconstruct(string $edge_id): EdgeInterface
  {
    $edge = Kernel::service("database")->get($edge_id);
    if(is_null($edge)) {
      throw new EdgeDoesNotExistException(sprintf("No edge with the id: %s", $edge_id));
    }
    $edge = unserialize($edge);
    if(!$edge instanceof EdgeInterface) {
      throw new NotAnEdgeException(sprintf("The id %s does not belong to a a valid edge entity.", $edge_id));
    }
    return $edge;
  }

  /**
   * Reconstructs all the connections (e.g. edges) for a given node and yields
   * as a \Generator.
   *
   * @param NodeInterface $node Node object implementing NodeInterface.
   *
   * @return  \Generator Yields edges filled with EdgeInterface objects.
   */
  public static function reconstructAll(NodeInterface $node): \Generator
  {
    $db = Kernel::service("database");
    $edges = $db->list_members(StringUtils::buildKey($node->getId(), Kernel::config()->root_edge_label));
    array_walk($edges, function($item, $key) use ($db) {
      yield unserialize($db->get($item));
    });
    if(count($edges)<1) yield;
  }

  /**
   * Reconstructs all the connections (e.g. edges) for a given node and returns
   * them in an array.
   *
   * !! changing this method to iterator_to_array($this->reconstructAll) won't
   *    work, because of an internal PHP bug:
   *    https://bugs.php.net/bug.php?id=60577
   *
   * @see reconstructAll
   *
   * @param NodeInterface $node Node object implementing NodeInterface.
   *
   * @return  array An array of edge objects implementing EdgeInterface.
   */
  public static function reconstructAllInArray(NodeInterface $node): array
  {
    $db = Kernel::service("database");
    $edges = $db->list_members(StringUtils::buildKey($node->getId(), Kernel::config()->root_edge_label));
    array_walk($edges, function(&$item, $key) use ($db) {
      $item = unserialize($db->get($item));
    });
    return $edges;
  }

}
