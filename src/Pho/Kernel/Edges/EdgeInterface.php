<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges;

use Pho\Kernel\Predicates\PredicateInterface;
use Pho\Kernel\Nodes\Abstracts\NodeInterface;

/**
 * An edge is like an arrow between two points. Its label is called
 * "the predicate".
 */
interface EdgeInterface {

  /**
   * Each edge gets a unique identifier (id) in cryptographically secure
   * UUID(v4) format. This method returns this particular edge's id.
   *
   * @return string The ID of the edge.
   */
  public function getId(): string;

  /**
   * Retrieves the source node of the edge. Each edge can have one and only one
   * source.
   *
   * @return NodeInterface The source node.
   */
  public function getFrom(): NodeInterface;

  /**
   * Retrieves the target node of the edge. Each edge can have one and only one
   * target.
   *
   * @return NodeInterface The target node.
   */
  public function getTo(): NodeInterface;

  /**
   * Retrieves the predicate of the edge. A predicate is like an entity type
   * for the edge.
   *
   * @return PredicateInterface The predicate object.
   */
  public function getPredicate(): PredicateInterface;

  /**
   * Destroys the object's internal values and deletes it from the database,
   * making it ready for an unset operation.
   */
  public function destroy(): void;

  /**
   * This method allows you to set the reciprocal edge of this particular edge.
   * It is intended to be run only at construction time, and for one time only.
   * Otherwise throws an exception. (The reason why it is not part of the
   * constructor itself is that the reciprocal edge may have not been built
   * by the time this object is being created.)
   *
   * @param Pho\Kernel\Edges\EdgeInterface  $edge Reciprocal edge object
   *
   * @throws CantChangeReciprocalException thrown when there is a reciprocal already set.
   */
  public function setReciprocal(self $edge): self;

  /**
   * Retrieves the reciprocal edge object.
   *
   * @return EdgeInterface The reciprocal edge.
   */
  public function getReciprocal(): self;

  /**
   * Retrieves the label of the edge. The edge label is initally set to be the
   * class name, but later can be changed to anything, or a human-readable
   * unique identifier.
   *
   * @return string The edge label.
   */
  public function getLabel(): string;

  /**
   * Sets up the label of the edge. The edge label is initally set to be the
   * class name, but later can be changed to anything, or a human-readable
   * unique identifier.
   *
   * @param string  $label
   */
  public function setLabel(string $label): void;


  public function connect(): void;
  public function isOrphan(): bool;
  public function isDirected(): bool;
  // attributes

}
