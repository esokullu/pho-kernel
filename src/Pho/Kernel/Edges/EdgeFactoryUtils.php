<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Edges;

use Pho\Kernel\Nodes\Abstracts\NodeInterface;
use Pho\Kernel\Nodes\NodeFactory;
use Pho\Kernel\Predicates\PredicateUtils;
use Pho\Kernel\Acl\AclBroker;

/**
 * Utility methods for the EdgeFactory class
 */
class EdgeFactoryUtils {

  /**
   * Commonly used Tools: ACL Broker for permissions.
   *
   * @var AclBroker
   */
  private static $acl_broker;
  
  /**
   * Commonly used Tools: Predicate Utils for predicate manipulation.
   *
   * @var PredicateUtils
   */
  private static $predicate_utils;

  /**
   * Checks whether there exists an edge between two nodes with the given predicate
   *

   * @param NodeInterface $node1 First node.
   * @param NodeInterface $node2 Second node.
   * @param string  $predicate Predicate sought. Empty if seeking any predicates.
   *
   * @return bool
   */
  protected static function edgeExists(NodeInterface $node1, NodeInterface $node2, string $predicate = ""): bool
  {
    $cnx = $node1->getEdgesBetween($node2);
    if(!empty($predicate))
      return count($cnx) > 0 && array_key_exists($predicate, $cnx);
    else
      return count($cnx) > 0;
  }


  /**
   * Fetches an edge with the given predicate between the two given nodes.
   * This is similar to EdgeFactory::reconstruct method, but instead of
   * bringing the edge back with the edge id, it does with subject node id,
   * object node id and the predicate label.
   *
   * @param NodeInterface $subject Source node.
   * @param NodeInterface $object Target node.
   * @param string  $predicate Predicate label.
   *
   * @return EdgeInterface|null The edge object if it exists, or null.
   */
  public static function getEdge(NodeInterface $subject, NodeInterface $object, string $predicate):  ?EdgeInterface
  {
    $cnx = $subject->getEdgesBetween($object);

    if(count($cnx)<1)
      return null;

    foreach($edges as $edge) {
      if($edge->getPredicate()->getLabel()==strtolower($predicate))
        return $edge;
    }

    return null;
  }


  /**
   * Functions the same as getEdge method, but uses IDs instead of node objects
   * to retrieve the edge.
   *
   * @see getEdge
   *
   * @param string $subject_id Source node's ID.
   * @param string $object_id Target node's ID.
   * @param string  $predicate Predicate label.
   *
   * @return EdgeInterface|null The edge object if it exists, or null.
   */
  public static function getEdgeById(string $subject_id, string $object_id, string $predicate):  ?EdgeInterface
  {
    $subject = NodeFactory::reconstruct($subject_id);
    $object = NodeFactory::reconstruct($object_id);
    return self::getEdge($subject, $object, $predicate);
  }

  /**
   * Fetches the edge and its parents in array form. This is similar to EdgeFactoryUtils::getEdge
   * in the sense that it seeks the edge not with its edge id but other parameters
   * that help with the search, but instead of returning just the single sought edge
   * it returns an array that also contains its ancestors.
   *
   * @param NodeInterface $subject Source node.
   * @param NodeInterface $object Target node.
   * @param string  $predicate Predicate label.
   *
   * @return array The edge objects in array.
   */
  public static function getEdges(NodeInterface $subject, NodeInterface $object, string $predicate): array
  {
    $edge = $this->getEdge($subject, $object, $predicate);
    if(is_null($edge)) return [];

    $edges = [$edge];

    if(count($edge)==1) return $edges;

    foreach(OoUtils::getAncestors(get_class($edge), false) as $ancestor_edge) {
      $edges[] = self::getEdge($subject, $object, $ancestor_edge->getPredicate()->getLabel());
    }

    return $edges;
  }

  /**
   * Functions the same as getEdges method, but uses IDs instead of node objects
   * to retrieve the edge.
   *
   * @see getEdges
   *
   * @param string $subject_id Source node's ID.
   * @param string $object_id Target node's ID.
   * @param string  $predicate Predicate label.
   *
   * @return array The edge objects in array.
   */
  public static function getEdgesById(string $subject_id, string $object_id, string $predicate):  array
  {
    $subject = NodeFactory::reconstruct($subject_id);
    $object = NodeFactory::reconstruct($object_id);
    return self::getEdges($subject, $object, $predicate);
  }

  protected static function predicateUtils(): PredicateUtils
  {
    if(is_null(self::$predicate_utils))
      self::$predicate_utils = new PredicateUtils();
    return self::$predicate_utils;
  }

  protected static function aclBroker(): AclBroker
  {
    if(is_null(self::$acl_broker))
      self::$acl_broker = new AclBroker(self::predicateUtils());
    return self::$acl_broker;
  }

}
