<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Exceptions;

/**
 * Thrown when the kernel is attempted to be extended with an
 * entity that doesn't meet official inteface guidelines and formatting.
 *
 * @author Emre Sokullu
 */
class InvalidEntityException extends \Exception {

}
