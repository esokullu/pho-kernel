<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Exceptions;

/**
 * Thrown when there's an attempt to create a service that doesn't exist.
 *
 * @author Emre Sokullu
 */
class ServiceUnavailableException extends \Exception {

}
