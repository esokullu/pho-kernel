<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Predicates\Exceptions;

/**
 * Thrown when the edge predicate is invalid, not registered.
 *
 * @author Emre Sokullu
 */
class InvalidPredicateException extends \Exception {

}
