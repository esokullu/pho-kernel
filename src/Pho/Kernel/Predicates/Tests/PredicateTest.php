<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Predicates\Tests;

use Pho\Kernel\Predicates\AbstractPredicate;

class PredicateTest extends \PHPUnit_Framework_TestCase {

  private $predicate;

  public function setUp() {
    $this->predicate = new class("contains") extends AbstractPredicate {};
  }

  public function tearDown() {
    unset($this->predicate);
  }

  public function testGetLabel() {
    $key = $this->predicate->getLabel();
    $expected = "contains";
    $this->assertEquals($expected, $key); // or assertRegExp
  }

  public function testHasReciprocal() {
    $has_reciprocal = $this->predicate->hasReciprocal();
    $this->assertTrue($has_reciprocal);
  }

  public function testGetReciprocal() {
    $reciprocal = $this->predicate->getReciprocal();
    $expected  = "belongs";
    $this->assertEquals($expected, $reciprocal->getLabel());
  }

  public function testGetEdgeClass() {
    $edge_class = $this->predicate->getEdgeClass();
    $expected = "Pho\\Kernel\\Edges\\Contains";
    $this->assertEquals($expected, $edge_class);
  }

}
