<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Predicates;

use Pho\Kernel\Kernel;
use Pho\Kernel\Predicates\Exceptions\InvalidPredicateException;

abstract class AbstractPredicate implements PredicateInterface  {

  /**
   * @var Pho\Kernel\Predicates\Utils
   */
  protected $utils;


  /**
   * @var string
   */
  protected $label;

  /**
   * @var string
   */
  protected $reciprocal;

  public function __construct(string $label) {
    $this->label = strtolower($label);
    $this->utils = new PredicateUtils();
    if(!isset($this->label) || !$this->utils->isValidPredicate($this->label)) {
      throw new InvalidPredicateException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasReciprocal(): bool
  {
    return $this->utils->hasReciprocal($this->label);
  }

  /**
   * {@inheritdoc}
   */
  public function getReciprocal(): PredicateInterface
  {
    $reciprocal  = $this->utils->getReciprocal($this->label);
    return new class($reciprocal) extends AbstractPredicate { };
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string
  {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function isBinding(): bool
  {
    return $this->utils->isBindingPredicate($this->getLabel());
  }

  /**
   * {@inheritdoc}
   */
  public function getEdgeClass(): string
  {
    return Kernel::config()->namespaces->edges . ucfirst($this->getLabel());
  }

  /**
   * This function is required for phpunit to work properly with anonymous
   * classes.
   *
   * @internal
   */
  public function __toString(): string
  {
    return (string) __CLASS__;
  }

}
