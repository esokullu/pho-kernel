<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Predicates;

use Pho\Kernel\Kernel;
use Pho\Kernel\Predicates\CorePredicates;

/**
 * Provides helper methods to the Predicate classes.
 *
 * @author Emre Sokullu
 */
class PredicateUtils {

  /**
   * @var array
   */
  protected $valid_predicates;

  /**
   * A set of predicates with their reciprocals.
   * 
   * @var array
   */
  protected $reciprocals = [
    CorePredicates::CONTAINS => CorePredicates::BELONGS,
    CorePredicates::SUBSCRIBES => CorePredicates::PUBLISHES, // Exec
    CorePredicates::CREATES => CorePredicates::PRODUCT, // write
    CorePredicates::TRANSMITS => CorePredicates::CONSUMES // read (transmit part of create and it is write)
  ];

  /**
   * Predicates that must edge with an Actor.
   * If value is true, the source is the Actor.
   * Otherwise, the destination is the Actor.
   *
   * @var array
   */
  protected $actor_predicates = [
    CorePredicates::SUBSCRIBES => true ,
    CorePredicates::PUBLISHES => false, 
    CorePredicates::CREATES => true,
    CorePredicates::PRODUCT => false, 
  ];

  /**
   * Predicates that, once deleted, create a special 
   * binding situation for their connections.
   * 
   * @var array
   */
  protected $binding_predicates = [
    CorePredicates::CONTAINS, CorePredicates::CREATES
  ];

  public function __construct() {
    $ref = new \ReflectionClass(CorePredicates::class);
    $this->valid_predicates = array_values($ref->getConstants());
  }

  /**
   * Checks if the given predicate is a valid, kernel-recognized one.
   *
   * @todo Make the list extensible.
   *
   * @param string  $predicate  The predicate label.
   *
   * @return bool Whether predicate is valid or not.
   */
  public function isValidPredicate(string $predicate): bool
  {
     return in_array(strtolower($predicate), $this->valid_predicates);
  }

  /**
   * Checks if the given predicate is binding, which means once it's deleted
   * all the edges it is connected *TO* must also be deleted.
   *
   * @todo Make the list extensible.
   *
   * @param string $predicate The predicate label.
   *
   * @return bool Whether the given predicate is indicated to be binding.
   */
  public function isBindingPredicate(string $predicate): bool
  {
    return in_array(strtolower($predicate), $this->binding_predicates);
  }

  public function hasActor(string $predicate): bool
  {
    return in_array(strtolower($predicate), array_keys($this->actor_predicates));
  }

  //public function getActingPredicate():

  public function getReciprocal(string $key): ?string
  {
    $key = strtolower($key);
    $search = array_search($key, $this->reciprocals);
    if($search!==false) {
      return $search;
    }
    else if(array_key_exists($key, $this->reciprocals)) {
      return $this->reciprocals[$key];
    }
    else return null;
  }

  public function hasReciprocal(string $key): bool
  {
    $key = strtolower($key);
    return (in_array($key, $this->reciprocals) || array_key_exists($key, $this->reciprocals));
  }

  public function toEdge(string $predicate): string
  {
    $predicate = strtolower($predicate);
    if(!$this->isValidPredicate($predicate)) {
      throw new InvalidPredicateException(sprintf("\"%s\" is not a valid predicate.", $predicate));
    }
    return Kernel::config()->namespaces->edges . ucfirst($predicate);
  }

}
