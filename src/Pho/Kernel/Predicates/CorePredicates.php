<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Predicates;

/**
 * Contains a list of predicates that are core to the kernel.
 *
 * @author Emre Sokullu
 */
class CorePredicates {

  const CONTAINS = "contains"; // parent
  const BELONGS = "belongs"; // child

  const SUBSCRIBES = "subscribes"; // subscriber`
  const PUBLISHES = "publishes"; // publisher

  const CREATES = "creates"; //author
  const PRODUCT = "product"; // product

  const TRANSMITS = "transmits"; // transmittee
  const CONSUMES = "consumes"; // consumer

}
