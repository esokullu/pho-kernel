<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Predicates;

interface PredicateInterface {

  /**
   * Checks if the predicate has a reciprocal version
   * of itsef. For example, contains and belongs are reciprocal
   * predicates.
   *
   * @return bool
   */
  public function hasReciprocal(): bool;

  /**
   * Returns the reciprocal predicate. For example, the
   * reciprocal of "belongs" predicate is "contains"
   *
   * @return Pho\Kernel\Predicates\PredicateInterface
   */
  public function getReciprocal(): self;

  /**
   * Fetches the predicate key in string format. This is
   * the format used to recognize the type of a serialized predicate.
   *
   * @return string
   */
  public function getLabel(): string;

  /**
   * Returns the Edge class attributable to this particular
   * predicate
   *
   * @return string Edge class name.
   */
  public function getEdgeClass(): string;


  /**
   * Checks if the given predicate is binding, which means once it's deleted
   * all the edges it is connected *TO* must also be deleted.
   *
   * @todo Make the list extensible.
   *
   * @param string $predicate The predicate label.
   *
   * @return bool Whether the given predicate is indicated to be binding.
   */
  public function isBinding(): bool;

}
