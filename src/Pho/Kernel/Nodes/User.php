<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\{Group, NodeFactory};
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Nodes\Abstracts\{AbstractActor, AbstractGraph, NodeInterface};
use Pho\Kernel\Predicates\CorePredicates;

/**
 * Main Actor class, subscribes to other nodes, and creates new Objects, Graphs.
 *
 * @author Emre Sokullu
 */
class User extends AbstractActor implements UserInterface  {

  /**
   * @var Pho\Kernel\Nodes\Abstracts\AbstractGraph
   */
   private $context;

  public function __construct(string $full_name = "") {
    parent::__construct();
    if(!empty($full_name))
      $this->setFullName($full_name);
  }

  /**
   * {@inheritdoc}
   */
  public function setFullName(string $full_name): void
  {
    $this->setAttribute("full_name", $full_name);
  }

  /**
   * {@inheritdoc}
   */
  public function getFullName(): string 
  {
    return (string) $this->getAttribute("full_name");
  }

  /**
   * {@inheritdoc}
   */
  public function follow(User $user): void
  {
    $this->subscribe($user);
    /*if(is_null($this->following)) {
      $this->following = new VirtualGraph( sprintf("Actor.%s.Following", $this->getId()) );
    }
    $this->join($this->following);*/
    Kernel::events()->emit("user.followed", [$this->getId(), $user->getId()]);
  }

  /**
   * {@inheritdoc}
   */
  /*
  public function postTo(Content $content, AbstractGraph $graph): void
  {
    $this->create($content);
    $content->transmit($graph);
    $content->save();
    Kernel::events()->emit("content.posted", [$this->getId(), $content->getId()]);
  }*/

/*
  public function reactTo(Reaction $reaction, Content $content): void
  {
    $this->create($reaction);
    $reaction->transmit($content);
    $content->save();
    Kernel::events()->emit("user.reacted", [$this->getId(), $content->getId()]);
  }*/

  public function createGroup(string $title, string $description): Group
  {
    $group = NodeFactory::build("group");
    $group->setTitle($title);
    $group->setDescription($description);
    $this->create($group);
    $this->join($group);
    Kernel::events()->emit("group.created", [$this->getId(), $group->getId()]);
    return $group;
  }

  /**
   * {@inheritdoc}
   */
  public function join(AbstractGraph $group): void
  {
    /*if($group->hasMember($this)) {
      
    }*/
    $this->subscribe($group);
    Kernel::events()->emit("group.joined", [$this->getId(), $group->getId()]);
  }

  /**
   * {@inheritdoc}
   */
  public function leave(Group $group): void
  {
    $this->unsubscribe($group);
    Kernel::events()->emit("group.left", [$this->getId(), $group->getId()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getGroups(string $order_by="", bool $exclude_created=false): array
  {
    $groups = [];
    foreach($this->getSubscriptions() as $subscription) {
      if($subscription instanceof AbstractGraph) {
        $groups[] = $subscription;
      }
    }
    if(!$exclude_created)
      return $groups;

    $created_groups = iterator_to_array($this->getCreatedGroups($order_by));

    return array_diff($groups, $created_groups);
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedGroups(string $order_by=""): array
  {
    $groups = [];
    foreach($this->getProducts() as $creation) {
      if($creation instanceof Group) {
        $groups[] = $creation;
      }
    }
    return $groups;
  }

  /**
   * {@inheritdoc}
   */
  public function getPosts(string $order_by=""): array
  {
    $content = [];
    foreach($this->getProducts() as $creation) {
      if($creation instanceof Content) {
        $content[] = $creation;
      }
    }
    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public function getFollows(string $order_by=""): array
  {
    $follows = [];
    foreach($this->getSubscriptions() as $subscription) {
      if($subscription instanceof Group) {
        $follows[] = $subscription;
      }
    }
    return $follows;
  }

  /**
   * {@inheritdoc}
   */
  public function getFollowers(string $order_by=""): array
  {
    $followers = [];
    foreach($this->getSubscribers() as $subscriber) {
      if($subscriber instanceof User) {
        $followers[] = $subscriber;
      }
    }
    return $followers;
  }

  /**
   * {@inheritdoc}
   * @todo implement $order_by
   */
  public function getMutualFollows(string $order_by): array
  {
    $follows = $this->getFollows();
    $followers = $this->getFollowers();
    return array_merge($follows, $followers);
  }

  /**
   * {@inheritdoc}
   */
  public function isFollowedBy(User $user): bool
  {
    $following = $this->filterConnections($this->getEdgesBetween($user), CorePredicates::PUBLISHES);
    return count($following) >= 1;
  }

  /**
   * {@inheritdoc}
   */
  public function isFollowing(User $user): bool
  {
    $following = $this->filterConnections($this->getEdgesBetween($user), CorePredicates::SUBSCRIBES);
    return count($following) >= 1;
  }

  /**
   * {@inheritdoc}
   */
  public function didPublish(Content $content): bool
  {
    $published = $this->filterConnections($this->getEdgesBetween($content), CorePredicates::CREATES);
    return count($published) >= 1;
  }

  /**
   * {@inheritdoc}
   */
  public function isMember(Group $group): bool
  {
    $subscribes = $this->filterConnections($this->getEdgesBetween($group), CorePredicates::SUBSCRIBES);
    return count($subscribes) >= 1;
  }


  public function context(): AbstractGraph
  {
    if(is_null($this->context)) {
      $this->resetContext();
    }
    return $this->context;
  }

  public function switchContext(AbstractGraph $context): void
  {
    $this->context = $context;
  }

  public function resetContext(): void
  {
    $this->switchContext(
      NodeFactory::reconstruct(
        Kernel::config()->root_graph_key
      )
    );
  }


}
