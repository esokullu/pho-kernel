<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

use Pho\Kernel\Nodes\Abstracts\AbstractGraph;

interface UserInterface {

    /**
     * Returns the full name of the user.
     * 
     * @return string The full name of the user.
     */
    public function getFullName(): string;

    /**
     * Sets the full name of the user.
     * 
     * @param string $full_name
     * @return void
     */
    public function setFullName(string $full_name): void;

    /**
     * Subscribes to the updates of the given user.
     *
     * @param User  $user The user to subscribe.
     */
    public function follow(User $user): void;

    /**
     * Nullifies any transmissions from the given user.
     *
     * @param User  $user The user to ban.
     */
    // public function ban(User $user): void;

    /**
     * Publishes content to the given graph (a group or the network itself.)
     *
     * @param Content $content The content object.
     * @param AbstractGraph $graph The graph to publish the content to.
     */
    //public function postTo(Content $content, AbstractGraph $graph): void;

    /**
     * Reacts to the given content.
     *
     * @param Content $reaction The reaction object.
     * @param Content $content  The content object to react.
     */
    //public function reactTo(Reaction $reaction, Content $content): void;

    /**
     * Publishes a group object.
     *
     * @param string  $title  The title of the group to create.
     * @param string  $description  The description of the group to create.
     */
    public function createGroup(string $title, string $description): Group;

    /**
     * Joins an existing group.
     *
     * @param Group $group   The group object to join.
     */
    public function join(AbstractGraph $group): void;

    /**
     * Leaves the group.
     *
     * @param Group $group   The group to leave in object form.
     */
    public function leave(Group $group): void;

    /**
     * Lists all the groups that the User is a member of.
     *
     * @param string $order_by  The order to retrieve the groups
     * @param bool  $exclude_created  If true, excludes the groups that the User has created.
     *
     * @return  array The group array. Empty or members will be Group objects.
     */
    public function getGroups(string $order_by, bool $exclude_created): array;

    /**
     * Retrieves only the groups that the User has created.
     *
     * @param string $order_by  The order to retrieve the groups
     *
     * @return  array The group array. Empty or members will be Group objects.
     */
    public function getCreatedGroups(string $order_by): array;


    /**
     * Lists all the content that the User has created
     *
     * @param string $order_by  The order to retrieve the groups
     * @param bool  $exclude_created  If true, excludes the groups that the User has created.
     *
     * @return  array The group array. Empty or members will be Group objects.
     */
    public function getPosts(string $order_by): array;

    /**
     * Retrieves a list of all Users who follow this particular User.
     *
     * @param string $order_by  How to sort the Users.
     *
     * @return array An empty array or an array of User objects.
     */
    public function getFollowers(string $order_by): array;

    /**
     * Retrieves a list of all Users that this particular User follows.
     *
     * @param string $order_by  How to sort the Users.
     *
     * @return array An empty array or an array of User objects.
     */
    public function getFollows(string $order_by): array;

    /**
     * Retrieves a list of all Users with mutual follow connection to this particular User.
     * In some context, this may be called a friendship relationsip.
     *
     * @param string $order_by  How to sort the Users.
     *
     * @return array An empty array or an array of User objects.
     */
    public function getMutualFollows(string $order_by): array;

    /**
     * Queries whether this particular User is followed by the given User.
     *
     * @param User  $user The user object to query.
     *
     * @return bool
     */
    public function isFollowedBy(User $user): bool;

    /**
     * Queries whether this particular User is following the given User.
     *
     * @param User  $user The user object to query.
     *
     * @return bool
     */
    public function isFollowing(User $user): bool;

    /**
     * Queries whether this particular User has published the given content.
     *
     * @param Content  $content The content object to query.
     *
     * @return bool
     */
    public function didPublish(Content $content): bool;

    /**
     * Queries whether this particular User is a member of the given Group.
     *
     * @param Group $group  Group object to query.
     *
     * @return bool
     */
    public function isMember(Group $group): bool;

}
