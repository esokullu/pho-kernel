<?php

class VirtualGraph extends AbstractGraph {
    /**
   * Group is derivable but Network is not, hence AbstractGraph is not.
   */
  use DerivableTrait;

  public function __construct() {
    parent::__construct();
    $this->acl()->chmod(017000); // NO editors
    $this->acl()->umask(00777); // he can't let others see or manipulate
    $this->initializeNode()->save();
    Kernel::service("events")->emit("node.created", [
      (string) $this->getId(), (string) $this->getLabel()
    ]);
  }
}