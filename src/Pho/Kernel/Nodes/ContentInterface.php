<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

use Pho\Kernel\Nodes\UserInterface;

interface ContentInterface {


  // public function setTitle(string $title): void;
  // public function getTitle(): string;

  /**
   * Adds $content to the database, timestamps it.
   *
   * @param string  $content  Content to write to the database.
   */
  public function write(string $content): void;

  /**
   * Retrieves the contents.
   *
   * @return  string Content associated with this Object node.
   */
  public function read(): string;


}
