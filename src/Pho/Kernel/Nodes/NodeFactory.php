<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

use Pho\Kernel\{Kernel};
use Pho\Kernel\Utils\StringUtils;
use Pho\Kernel\Nodes\Abstracts\NodeInterface;
use Pho\Kernel\Nodes\Exceptions\NodeDoesNotExistException;

/**
 * A factory class responsible of building new nodes or fetching existing ones
 * from the database.
 *
 * @author Emre Sokullu
 */
class NodeFactory {

  /**
   * @var array
   */
  private static $internal_db = [];

  /**
   * Given the node uuid, we initially don't know what label the node
   * pertains to. This function returns the node in object form
   * with its correct label.
   *
   * @param string  $id the uuid of the node.
   *
   * @return NodeInterface  the node in its object form.
   *
   * @throws NodeDoesNotExistException When there is no entity with the given id.
   * @throws NotANodeException When the given id does not belong to a node.
   */
  public static function reconstruct(string $id): NodeInterface
  {
    Kernel::service("logger")->info (
      sprintf("Fetching node object with id \"%s\"", $id)
    );
    $node = Kernel::service("database")->get($id);
    if(is_null($node)) {
      throw new NodeDoesNotExistException(sprintf("There is no node registered with the uuid %s", $id));
    }
    $node = unserialize($node);
    if(!$node instanceof NodeInterface) {
      throw new NotANodeException("The id \"%s\" does not pertain to a Node.");
    }
    self::$internal_db[$id] = $node;
    return $node;
  }

  /**
   * Builds a new node with the given label.
   *
   * @param string  $label Supposed to be the label name without the namespace but it's safe to use it with namespaces as well.
   *
   * @return NodeInterface The node object.
   */
  public static function build(string $label, ...$args): NodeInterface
  {
    $node = null;
    $class_name = Kernel::config()->namespaces->nodes . ucfirst(strtolower(StringUtils::trimNamespace($label)));
    if(!is_null($args) && count($args) > 0)
      $node = new $class_name(...$args);
    else
      $node = new $class_name();
    self::$internal_db[$node->getId()] = $node;
    return $node;
  }

}
