<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

interface GroupInterface {

    /**
     * Sets the title of the group. Must be short.
     *
     * @param string  $title
     */
    public function setTitle(string $title): void;

    /**
     * Sets the description of the group.
     *
     * @param string  $title
     */
    public function setDescription(string $description): void;


    /**
     * Groups may have a logo.
     *
     * @param string $file_path Path to the image file.
     */
    public function setLogo(string $file_path): void;

    /**
     * Retrieves the logo of the group.
     *
     * @return string The logo path.
     */
    public function getLogo(): string;


    /**
     * Returns a list of group members as User objects.
     *
     * @param string  $order_by
     *
     * @return array An array of User objects.
     */
    public function getMembers(string $order_by): array;

    /**
     * Returns a list of content.
     *
     * @param string  $order_by
     *
     * @return array An array of Content objects.
     */
    public function getContent(string $order_by): array;

    /**
     * Returns a list of subgroups.
     *
     * @param string  $order_by
     *
     * @return array An array of Group objects.
     */
    public function getSubgroups(string $order_by): array;

}
