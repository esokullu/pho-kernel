<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\{UserInterface, Content};
use Pho\Kernel\Nodes\Abstracts\{AbstractObject, AbstractGraph, AbstractActor, NodeInterface, DerivableTrait};

/**
 * Group is a general purpose Graph that brings a number of Actors together
 * and let them post Objects in a certain context.
 *
 * @author Emre Sokullu
 */
class Group extends AbstractGraph implements GroupInterface {

  /**
   * Group is derivable but Network is not, hence AbstractGraph is not.
   */
  use DerivableTrait;

  public function __construct() {
    parent::__construct();
    $this->acl()->chmod(007775); // NO sticky bit, editors can change settings, +w for members, 4 for context --they can find and see, or it could be 5 so they can also see members. 0  for a completely secret invite-only group.
    $this->acl()->umask(00002); // he can't let world edit content, but he can let them see or not the members inside or consume the content or not.
    $this->initializeNode()->save();
    Kernel::service("events")->emit("node.created", [
      (string) $this->getId(), (string) $this->getLabel()
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title): void
  {
    $this->setAttribute("title", $title)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $description): void
  {
    $this->setAttribute("description", $description)->save();
  }

  /**
   * {@inheritdoc}
   */
  public function setLogo(string $file_path): void
  {
    if($this->checkImage($file_path)) {
      Kernel::service("storage")->put($file_path, $this->getId()."-logo".image_type_to_extension(exif_imagetype($file_path)));
    }
  }

  /**
   * Checks if the given image is of valid type and size given the
   * configurations of the kernel.
   *
   * @param string $file_path
   *
   * @return  bool
   */
  private function checkImage(string $file_path): bool
  {
    list($width, $height, $type, $attr) = getimagesize($file_path);
    return (
      in_array($type, Kernel::config()->group_logo_constraints->format->toArray()) &&
      $width > Kernel::config()->group_logo_constraints->width->min &&
      $width < Kernel::config()->group_logo_constraints->width->max &&
      $height > Kernel::config()->group_logo_constraints->height->min &&
      $height < Kernel::config()->group_logo_constraints->height->max
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getLogo(): string
  {
    $s = &Kernel::service("storage");
    foreach(Kernel::conf()->group_logo_constraints->format->toArray() as $type) {
      $file = $this->getId()."-logo";
      if($s->file_exists($file.image_type_to_extension($type))) {
        return $s->get($file.image_type_to_extension($type));
      }
    }
    return "";
  }

  /**
   * {@inheritdoc}
   */
  public function getMembers(string $order_by): array
  {
    return $this->filterConnections($this->getEdgesFrom(), CorePredicates::PUBLISHES);
  }

  /**
   * {@inheritdoc}
   */
  public function getContent(string $order_by): array
  {
    return $this->filterConnections($this->getEdgesFrom(), CorePredicates::CONSUMES);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubgroups(string $order_by): array
  {
    return $this->filterConnectionsByAncestorClass($this->getContainees(), self::class);
  }

}
