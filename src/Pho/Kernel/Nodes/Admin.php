<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

use Pho\Kernel\Roles\BaseRoles;

class Admin extends User {
    public function __construct() {
        $this->setRole(BaseRoles::ADMIN);
        // $this->acl()->umask(0000);
        parent::__construct();
    }
}