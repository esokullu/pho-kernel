<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\Exceptions\NodeExistsException;
use Pho\Kernel\Nodes\Abstracts\{AbstractGraph, NodeInterface};

/**
 * Network is the root node, a Graph with a static ID. Since it is the main
 * graph itself, it contains all the nodes available. Note that, Network is a
 * **final** class, it is not extensible.
 *
 * @author Emre Sokullu
 */
final class Network extends AbstractGraph {


  

  /**
   * Forms the network node. Network node is the root node, contains all others.
   *
   * @throws NodeExistsException if the network node is already present.
   */
  public function __construct() {
    $id = Kernel::config()->root_graph_key;
    /*Kernel::service("logger")->info(
      sprintf("Constructing the network node with id: %s", $id)
    );*/
    if(is_null(Kernel::service("database")->get($id))) {
      $this->setId($id)->initializeNode()->save();
    }
    else {
      throw new NodeExistsException("The root node is already present.");
    }
    parent::__construct();
    $this->acl()->chmod(017774); // if no 4 for world, they can't even sign up. OCTAL, STICKY BIT, OWNER, GROUPS (EDITORS), SUBSCRIBERS (MEMBERS), CONTEXT (WORLD)
    //$this->acl()->umask(00); // FOR ACTORS.
    Kernel::service("events")->emit("node.created", [
      (string) $this->getId(), (string) $this->getLabel()
    ]);
  }

  /**
   * Logs in the given user and returns the associated User object.
   * 
   * @param string $user_id The node id.
   * 
   * @return User The User object.
   * 
   * @throws UserNotFoundException Occurs when the user_id does not match a relevant entry in the database.
   */
  public function logIn(string $user_id): User
  {
    try {
      $node = NodeFactory::reconstruct($user_id);
    } 
    catch(NodeDoesNotExistException $e) {
      throw new UserNotFoundException($user_id);
    }
    catch(NotANodeException $e) {
      throw new UserNotFoundException($user_id);
    }

    if(!$node instanceof User) {
      throw new UserNotFoundException($user_id);
    }
    return $node;
  }

  /**
   * Creates a new user with given parameters and returns the User object.
   * 
   * @param string $full_name Full name.
   * 
   * @return User The new user object.
   */
  public function signUp(string $full_name): User
  {
    $user = new User($full_name); // NodeFactory::build("user", ...$params);
    return $user;
  }

  public function handleNodeCreated(string $id, string $label): void
  {
    Kernel::service("logger")->info("node.created triggered and the listener has acted.");
    $node = NodeFactory::reconstruct($id);
    if($node instanceof AbstractGraph) {
      $this->contains($node);
    }
    else if($node instanceof User) {
      $node->join($this);
    }
  }


  

}
