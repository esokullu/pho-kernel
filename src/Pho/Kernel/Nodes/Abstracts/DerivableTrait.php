<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts;

use Pho\Kernel\Nodes\Abstracts\Exceptions\{DerivableNodeWithNoParentException, DerivableNodeWithMultipleParentsException};
use Pho\Kernel\Nodes\Exceptions\IncompatibleTypeException;

trait DerivableTrait {

  /**
   * Returns the node that contains this particular node.
   *
   * @return  AbstractGraph The parent node
   *
   * @throws DerivableNodeWithNoParentException Thrown when the node does not have any parents.
   * @throws DerivableNodeWithMultipleParentsException Thrown when the node has multiple parents.
   * @throws IncompatibleTypeException Thrown when the parent is not an AbstractGraph.
   */
  public function getParent(): AbstractGraph
  {
    $edges = $this->filterConnections(
      $this->getEdgesFrom(), CorePredicates::BELONGS
    );
    if(!is_array($edges)) {
        throw new DerivableNodeWithNoParentException($this);
    }
    else if(count($edges)!=1) {
      throw new DerivableNodeWithMultipleParentsException($this);
    }
    else if(!$edges[0] instanceof AbstractGraph) {
      throw new IncompatibleTypeException("A parent node can only be an instance of AbstractGraph.");
    }
    return $edges[0]->getTo();
  }

}
