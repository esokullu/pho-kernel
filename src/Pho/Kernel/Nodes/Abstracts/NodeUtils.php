<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts;

use Pho\Kernel\Predicates\{PredicateUtils, PredicateInterface};
use Pho\Kernel\Predicates\Exceptions\InvalidPredicateException;

class NodeUtils {

  private $predicate_utils;

  private function getPredicateUtils(): PredicateUtils
  {
    if(!isset($this->predicate_utils))
      $this->predicate_utils = new PredicateUtils();
    return $this->predicate_utils;
  }

  protected function filterConnections(array $connections, string $predicate): array
  {
    if(!$this->getPredicateUtils()->isValidPredicate($predicate)) {
      throw new InvalidPredicateException( sprintf("\"%s\" is not a valid predicate.", $predicate) );
    }
    array_walk($connections, function($item, $key) use (&$connections, $predicate) {
      if($item->getPredicate()->getLabel()!=$predicate) {
        unset($connections[$key]);
      }
    });
    return array_values($connections);
  }

  protected function filterConnectionsByAncestorClass(array $connections, string $class_name): array
  {
    if(!class_exists($class_name)) {
      throw new \Exception( sprintf("\"%s\" is not a valid class name.", $class_name) );
    }
    array_walk($connections, function($item, $key) use (&$connections, $predicate) {
      if(!$item instanceof $class_name) {
        unset($connections[$key]);
      }
    });
    return array_values($connections);
  }

}
