<?php

use Pho\Kernel\Kernel;
use Pho\Kernel\Predicates\PredicateUtils;
use Pho\Kernel\Edges\EdgeFactory;

trait ExpirableTrait {

  /**
   * Deletes this node and all associated entries within a certain time period.
   *
   * @param  int $seconds Time to expire this node within.
   */
  public function expire(int $seconds): void
  {
    // 1. Log operation
    Kernel::service("logger")->info(
      sprintf("About to set expiration time (%s seconds) for the node \"%s\"", (string) $seconds, $this->getId())
    );

    // 2. Expire this node and its edges.
    Kernel::service("database")->expire($this->getId(), $seconds);
    $edges = $this->getEdgesFrom();
    foreach($edges as $edge) {
      $edge->expire($seconds);
    }

    // 3. Make sure all edges re: to this node will also get expired.
    $predicate_utils = new PredicateUtils();
    $id = $this->getId();
    Kernel::events()->on("edge.created",
      function(string $edge_id, string $source_id, string $target_id, string $predicate_label) use ($id, $seconds, $predicate_utils) {
        if( $source_id === $id && $predicate_utils->isBindingPredicate($predicate_label)) {
          Kernel::service("database")->expire($target_id, $seconds); // target id only!
        }
        // not else if!!
        if( $source_id===$id || $target_id===$id ) {
          array_walk(EdgeFactory::getEdgesById($source_id, $target_id, $predicate_label),
            function($item, $key) use ($seconds) {
              Kernel::service("database")->expire($item->getId(), $seconds);
            }
          );
        }
      }
    );

    // 4. Emit the set for expiration signal.
    Kernel::events()->emit("node.set_for_expiration", [$this->getId(), $seconds]);

  }

  /**
   * Retrieves the remaining time for this object to live. Returns -1 if the
   * node is not set to expire.
   *
   * @return int Remaining life time in seconds or -1 if it doesn't expire.
   */
  public function getTTL(): int
  {
    return Kernel::service("database")->getTTL($this->getId());
  }


}
