<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts;

use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\NodeFactory;
use Pho\Kernel\Nodes\Abstracts\{AbstractNode, NodeInterface};
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Predicates\CorePredicates;
use Pho\Kernel\Nodes\UserInterface;
use Pho\Kernel\Nodes\Abstracts\Exceptions\{OrphanObjectException, ObjectWithMultipleCreatorsException};
use Pho\Kernel\Acl\ObjectAcl;

/**
 * Objects are created by Actors to be transmitted (or attached) to other nodes.
 *
 * @author Emre Sokullu
 */
abstract class AbstractObject extends AbstractNode {

  use DerivableTrait;

  /**
   * Who created this Object. Must be an Actor.
   *
   * @var string
   */
  protected $creator_id;

  /**
   * Where is this Object transmitted to? 
   *
   * @var array
   */
  protected $transmits_id = [];

  /**
   * @var ObjectAcl
   */
  protected $acl;

  public function __construct() {
    parent::__construct();
    $this->acl = new ObjectAcl();
    $this->initializeNode();
  }

  public function acl(): ObjectAcl
  {
    return $this->acl;
  }

  /**
   * Defines the creator of this Object.
   *
   * @param string $creator_id Must point to an Actor.
   * @return AbstractObject The object itself for chaining.
   */
  protected function setCreator(string $creator_id): AbstractObject
  {
    $this->creator_id = $creator_id;
    return $this;
  }

  public function getCreator(): AbstractActor
  {
    return NodeFactory::reconstruct($this->creator_id);
  }

  /**
   * Identifies the node(s) that this object transmits its data.
   * You may identify multiple nodes by calling this method multiple times.
   *
   * @param NodeInterface $node The node to transmit data.
   */
  public function transmit(NodeInterface $node): void
  {
    EdgeFactory::build($this, $node, CorePredicates::TRANSMITS);
  }

  /**
   * Executes the Transmit edges for this Object.
   * 
   * @throws OrphanObjectException when there is no transmission set up.
   */
  public function transmitAll(): void
  {
    if(count($this->transmits_id)<1) {
      throw new OrphanObjectException($this->getId());
    }
    foreach($this->transmits_id as $transmits_id) {
      $this->transmit(NodeFactory::reconstruct($transmits_id));
    }
  }

  /**
   * Sets up the edges that Identify the node(s) that this object transmits its data.
   * 
   * @see transmit
   * 
   * @param NodeInterface $node
   * 
   * @return AbstractObject
   */
  public function inContext(NodeInterface $node): AbstractObject
  {
    $this->transmits_id[] = $node;
    //$this->transmit($node); // The object has not been created yet.
    return $this; //->post();
  }

  /**
   * Ensures the object is set up properly and posts it to designated context.
   * 
   * @return AbstractObject
   */
  public function post(): AbstractObject
  {
   

    $this->save();

    try {
      $this->transmitAll();
    }
    catch(OrphanObjectException $e) {
      throw $e;
    }
    
    $this->getCreator()->create($this);

    try {
      Kernel::events()->emit("object.posted", [$this->getAuthor()->getId(), $this->getId()]);
    }
    catch(OrphanObjectException $e) {
      throw $e;
    }
    catch(ObjectWithMultipleCreatorsException $e) {
      throw $e;
    }

    return $this;

  }

  /**
   * Retrieves the author of the Object in question.
   *
   * @return AbstractAuthor The Actor object.
   * 
   * @throws OrphanObjectException thrown when the Object has no author.
   * @throws ObjectWithMultipleCreatorsException thrown when the Object has multiple authors.
   */
  public function getAuthor(): AbstractActor
  {
    $edges = $this->filterConnections($this->getEdgesFrom(), CorePredicates::PRODUCT);
    if(count($edges)==0)
        throw new OrphanObjectException($this->getId());
    else if (count($edges)>1){
        throw new ObjectWithMultipleCreatorsException($this->getId());
    }
    return $edges[0]->getTo();
  }

 /*
   * Yields a list of consumers of this object.
   * @see getConsumersAsArray
   *
   * @return \Generator The consumer(s)
   */
  public function getConsumers(): \Generator
  {
    $edges = $this->getEdgesFrom();
    foreach($edges as $edge) {
      if($edge->getPredicate()->getLabel() == CorePredicates::TRANSMITS) {
        yield $edge->getTo();
      }
    }
  }


  /**
   * Retrieves a list of consumer of this object as array.
   * @see getConsumers
   *
   * @return array The consumer(s).
   */
  public function getConsumersAsArray(): array
  {
    return iterator_to_array($this->getConsumers());
  }

}
