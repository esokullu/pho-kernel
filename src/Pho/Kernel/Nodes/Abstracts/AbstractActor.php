<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts;

use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\{UserInterface, NodeFactory};
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Predicates\CorePredicates;
use Pho\Kernel\Nodes\Exceptions\IncompatibleTypeException;

/**
 * Abstract classes only creates edges, deal with predicates.
 * Actors do two things; they create graph and object nodes, and subscribe to
 * other nodes.
 *
 * @author Emre Sokullu
 */
abstract class AbstractActor extends AbstractNode {

  use ConsumerTrait, DerivableTrait;

  /**
   * Keeps the roles of this actor.
   *
   * @var Roles
   */
  protected $roles;

  public function __construct() {
    parent::__construct();
    $this->initializeNode()->save();
    Kernel::service("events")->emit("node.created", [
      (string) $this->getId(), (string) $this->getLabel()
    ]);
  }

  public function roles(): RolesInterface
  {
    return $this->roles;
  }

  /**
   * Marks the given node as a product of this Actor. The given node can
   * be either an object or a graph.
   *
   * @param NodeInterface $node The node to mark "created". Must be an instance of
   *               AbstractObject or AbstractGraph.
   */
  public function create(NodeInterface $node) : void
  {
    /*
    $content = NodeFactory::build($type);*/
    if(!$node instanceof AbstractObject && !$node instanceof AbstractGraph) {
      throw new IncompatibleTypeException(
        sprintf("\"%s\" is not a compatible type to publish.", $node->getLabel()) // getLabel() because it must be a child of NodeInterface
      );
    }

    EdgeFactory::build($this, $node, CorePredicates::CREATES);
  }

  public function draft(string $label, ...$params): AbstractObject
  {
    array_unshift($params, $this->getId()); // define the Creator.
    $draft = NodeFactory::build($label, ...$params);
    if(!$draft instanceof AbstractObject) {
      throw new ImpossibleDraftTypeException($this->getId(), $label);
    }
    Kernel::events()->emit("object.drafted", [$this->getId(), $draft->getId()]);
    return $draft;
  }


  /**
   * Marks the node with given ID as consumed, and returns the object. This is
   * the recommended way of accessing objects in Pho Kernel.
   *
   * @todo Increment counter for a detached object of nodeconsume.
   *
   * @param string  $id
   *
   * @return NodeInterface  The node with the given id.
   */
  public function consume(string $id): NodeInterface
  {
    $node = NodeFactory::reconstruct($id);
    Kernel::service("logger")->info(
      sprintf("Actor %s is consuming node %s with label %s", $this->getId(), $id, $node->getLabel())
    );
    return $node;
  }

  /**
   * When subscribed, an actor commits to listen to updates from the given node.
   * Subscribing to an Actor => Follow, friend someone.
   * Subscribing to an Object => Subscribing to its updates.
   * Subscribing to a Graph => Becoming a member.
   *
   * @param NodeInterface $node The node to subscribe.
   */
  public function subscribe(NodeInterface $node) : void
  {
    EdgeFactory::build($this, $node, CorePredicates::SUBSCRIBES);
  }


  /**
   * The opposite of subscribing. Destroys connections recursively between
   * two nodes.
   *
   * @param NodeInterface $node The node to unsubscribe from.
   */
  public function unsubscribe(NodeInterface $node): void
  {
    EdgeFactory::destroy($this, $node, CorePredicates::SUBSCRIBES);
  }


  /**
   * Yields a list of subscriptions.
   * @see getSubscriptionsAsArray
   *
   * @return \Generator The subscriptions.
   */
  public function getSubscriptions(): \Generator
  {
    $edges = $this->getEdgesFrom();
    foreach($edges as $edge) {
      if($edge->getPredicate()->getLabel() == CorePredicates::SUBSCRIBES) {
        yield $edge->getTo();
      }
    }
  }

  /**
   * Retrieves a list of subscriptions as array.
   * @see getSubscriptions
   *
   * @return array The subscriptions.
   */
   public function getSubscriptionsAsArray(): array
   {
     return iterator_to_array($this->getSubscriptions());
   }

/**
 * Yields a list of subscribers.
 * @see getSubscribersAsArray
 *
 * @return \Generator Subscribers.
 */
  public function getSubscribers(): \Generator
  {
    $edges = $this->getEdgesFrom();
    foreach($edges as $edge) {
      if($edge->getPredicate()->getLabel() == CorePredicates::PUBLISHES) {
        yield $edge->getTo();
      }
    }
  }


  /**
  * Retrieves a list of subscribers as array.
  * @see getSubscribers
   *
   * @return array The subscribers.
   */
  public function getSubscribersAsArray(): array
  {
    return iterator_to_array($this->getSubscribers());
  }


  /**
   * Yields a list of products/creations.
   * @see getProductsAsArray
   *
   * @return \Generator The creations.
   */
  public function getProducts(): \Generator
  {
    $edges = $this->getEdgesFrom();
    foreach($edges as $edge) {
      if($edge->getPredicate()->getLabel() == CorePredicates::CREATES) {
        yield $edge->getTo();
      }
    }
  }

  /**
   * Retrieves a list of producs/creations as array.
   * @see getProducts
   *
   * @return array The creations.
   */
  public function getProductsAsArray(): array
  {
    return iterator_to_array($this->getProducts());
  }


}
