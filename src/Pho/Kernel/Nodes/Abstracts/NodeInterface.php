<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts;

interface NodeInterface {

  /**
   * Returns the unique id of the node.
   *
   * @return  String The UUID
   */
  public function getId(): string;


  /**
   * Returns the Label of the node.
   *
   * @return String The label in all lowercase.
   */
  public function getLabel(): string;

  /**
   * Saves the created object to the database.
   *
   * @return The node object itself, for chaining.
   */
   public function save(): self;

   /**
    * Returns the given attribute, or null if it is not set.
    *
    * @param string $attribute
    *
    * @return string|array|null
    */
   public function getAttribute(string $attribute);


   /**
    * Attribute setter.
    *
    * @param  string  $attribute Attribute key to set.
    * @param  string|array|bool $value  Value.
    */
   public function setAttribute(string $attribute, $value): NodeInterface;


   /**
    * Destroys the node, removing it from the database and unsetting
    * the object itself.
    *
    * @throws IndestructibleRootGraphException Thrown with an attempt to destroy the root graph.
    */
   public function destroy(): void;


   /**
    * Returns a list of all the edges (both to and from) pertaining to
    * this particular node.
    *
    * @return array An array of EdgeInterface objects.
    */
   public function getEdges(): array;

   /**
    * Returns a list of all the edges directed towards
    * this particular node.
    *
    * @return array An array of EdgeInterface objects.
    */
   public function getEdgesTo(): array;


   /**
    * Returns a list of all the edges originating from
    * this particular node.
    *
    * @return array An array of EdgeInterface objects.
    */
   public function getEdgesFrom(): array;


   /**
    * Retrieves a list of edges between this node and the target node specified
    * in the parameters.
    *
    * @param NodeInterface  $node Target node.
    *
    * @return array An array of edge objects in between, with edge label as the key. Returns an empty array if there is no connections in between.
    */
   public function getEdgesBetween(NodeInterface $node): array;

   
   /**
    * This is an alias to the getCreator() method.
    * @see getCreator
    *
    * @return ?AbstractActor The creator Actor object, if available, or null.
    */
   public function getOwner(): ?AbstractActor ;

  /**
    * Retrieves the creator Actor of this node. Returns null
    * if the node has no creator (e.g. Network node.)
    *
    * @return ?AbstractActor The creator Actor object, if available, or null.
    */
   public function getCreator(): ?AbstractActor ;

}
