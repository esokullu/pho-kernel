<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts;

trait ConsumerTrait {
  public function consume(AbstractObject $object): void
  {
    EdgeFactory::build($this, $object, CorePredicates::CONSUMES);
  }
}
