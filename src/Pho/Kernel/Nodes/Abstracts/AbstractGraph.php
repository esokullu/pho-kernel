<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts;

use Pho\Kernel\Nodes\Abstracts\AbstractNode;
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Predicates\CorePredicates;
use Pho\Kernel\Exceptions\ImpossibleContaineeException;
use Pho\Kernel\Acl\GraphAcl;

/**
 * Graphs contain other nodes.
 *
 * @author Emre Sokullu
 */
abstract class AbstractGraph extends AbstractNode {

  use ConsumerTrait;

  /**
   * @var GraphAcl
   */
  protected $acl;

  public function __construct() {
    parent::__construct();
    $this->acl = new GraphAcl();
  }

  /**
   * Adds the given node to the graph object. Makes the given node a containee.
   *
   * @param AbstractNode  $node The node object to contain.
   */
  public function contains(AbstractGraph $node) : void
  {
    if($node instanceof AbstractGraph) {
      EdgeFactory::build($this, $node, CorePredicates::CONTAINS);
    }
    else {
      throw new ImpossibleContaineeException($this, $node);
    }
  }

  public function acl(): GraphAcl
  {
    return $this->acl;
  }

  /**
   * Retrieves the list of all nodes contained by this graph as array.
   *
   * @return array The list of nodes to contain.
   */
  public function getContainees(): array
  {
    $edges = $this->filterConnections(
      $this->getEdgesFrom(), CorePredicates::CONTAINS
    );
    $returns = [];
    array_walk($edges, function($item, $key) use (&$returns) {
      $returns[] = $item->getTo();
    });
    return $returns;
  }

  /**
   * Retrieves the list of node IDs contained by this graph as an array
   *
   * * @return array The list of node IDs to contain.
   */
  public function getContaineeIds(): array
  {
    $containees = $this->getContainees();
    array_walk($containees, function(&$item, $key) {
      $item = $item->getId();
    });
    return $containees;
  }

}
