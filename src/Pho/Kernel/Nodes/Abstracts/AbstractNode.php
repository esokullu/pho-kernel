<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts;

use Pho\Kernel\Kernel;
use Pho\Kernel\Utils\StringUtils;
use Pho\Kernel\Nodes\Abstracts\NodeInterface;

use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Nodes\Exceptions\{LabelMismatchException, NodeDoesNotExistException};
use Pho\Kernel\Predicates\CorePredicates;

abstract class AbstractNode extends NodeUtils implements NodeInterface, \Serializable {

 /**
  * @var string
  */
  protected $id;

  /**
   * @var array
   */
   protected $data;




  /**
   * Node constructor.
   */
  public function __construct() {
    Kernel::service("logger")->info(
      sprintf("Creating a node object with id \"%s\" and label \"%s\"", $this->getId(), $this->getLabel())
    );
  }

  /**
    * Initializes a new node.
    *
    * When a node is formed for the first time,
    * this function is responsible of forming the first
    * properties of the object.
    *
    * @return self 
    */
  protected function initializeNode(): self
  {
    /*Kernel::service("logger")->info(
      sprintf("Initializing a new node object with label \"%s\"", $this->getLabel())
    );*/
    if(empty($this->getId()))
      $this->setId(StringUtils::genUUID());

    $this->data = [
      "label"=>$this->getLabel(),
      "attributes"=>[
        "created_at" => time()
      ]
    ];

    return $this;
  }

  public function serialize(): string
  {
    return serialize([
      "id" => $this->id,
      "data" => $this->data
    ]);
  }

  public function unserialize($data): void
  {
    $data = unserialize($data);
    $this->id = $data["id"];
    $this->data = $data["data"];
  }


  /**
   * {@inheritdoc}
   */
  public function getId(): string
  {
    return (string) $this->id;
  }

  /**
   * Sets the id of the node after initialization.
   *
   * @param string  $id
   * @return self
   */
  protected function setId(string $id): self
  {
    $this->id = $id;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getAttribute(string $attribute)
  {
    if(isset($this->data["attributes"][$attribute])) {
      return $this->data["attributes"][$attribute];
    }
    else {
      return null;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setAttribute(string $attribute, $value): NodeInterface
  {
    if(is_bool($value) || is_array($value))
      $this->data["attributes"][$attribute] = $value;
    else
      $this->data["attributes"][$attribute] = (string) $value;

      if(Kernel::isToIndex($this->getLabel(), $attribute)) {

      }

      Kernel::events()->emit("node.modified", [$this->getId(), $this->getLabel(), $attribute]);

      return $this->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string
  {
    $label = StringUtils::trimNamespace(get_class($this));
    Kernel::service("logger")->info(
      sprintf("%s called. The class name is: %s", __METHOD__, $label)
    );
    return strtolower($label);
  }

  /**
   * {@inheritdoc}
   */
  public function save(): NodeInterface
  {
    Kernel::service("logger")->info(
      sprintf("About to save the node \"%s\" with the data: %s", $this->getId(), json_encode($this->data))
    );
    Kernel::service("database")->set(
      $this->getId(), serialize($this)
    );
    /*
    Kernel::service("events")->emit("node.created", [
      (string) $this->getId(), (string) $this->getLabel()
    ]);
    */
    return $this;
  }

  public function __toString(): string
  {
    return $this->getId();
  }

  /**
   * {@inheritdoc}
   */
  public function destroy(): void
  {
    Kernel::service("logger")->info(
      sprintf("Destroying the node \"%s\", a \"%s\"", $this->getId(), $this->getLabel())
    );
    if($this->getLabel()==Kernel::config()->root_graph_label) {
      throw new IndestructibleRootGraphException();
    }
    $edges = $this->getEdgesFrom();
    foreach($edges as $edge) {
      $edge->destroy();
    }
    Kernel::service("database")->delete($this->getId());
  }

  /**
   * {@inheritdoc}
   */
  public function getEdges(): array
  {
    return array_merge($this->getEdgesFrom(), $this->getEdgesTo());
  }

  /**
   * {@inheritdoc}
   */
  public function getEdgesTo(): array
  {
    $cnx = $this->getEdgesFrom();
    array_walk($cnx, function(&$item, $key) {
      if($item->isDirected())
        $item = $item->getReciprocal();
    });
    return $cnx;
  }

  /**
   * {@inheritdoc}
   */
  public function getEdgesFrom(): array
  {
    return EdgeFactory::reconstructAllInArray($this);
  }

  /**
   * {@inheritdoc}
   */
  public function getEdgesBetween(NodeInterface $node): array
  {
    $cnx = $this->getEdgesFrom();
    $edges_between = [];
    array_walk($cnx, function(&$item, $key) use ($node, &$edges_between) {
      if($item->getTo()->getId() == $node->getId())
        $edges_between[$item->getPredicate()->getLabel()] = $item;
    });
    return $edges_between;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): ?AbstractActor 
  {
    return $this->getCreator();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreator(): ?AbstractActor
  {
    $edges = $this->getEdgesFrom();
    $return = null;
    array_walk($edges, function($item, $key) use (&$return) {
      if($item->getPredicate()->getLabel()==CorePredicates::PRODUCT) {
        $return = $item->getTo();
        return;
      }
    });
    return $return;
  }


}
