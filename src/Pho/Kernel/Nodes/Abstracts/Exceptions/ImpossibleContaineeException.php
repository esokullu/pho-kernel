<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts\Exceptions;

use Pho\Kernel\Nodes\Abstracts\NodeInterface;

/**
 * Thrown with an attempt to contain an Object.
 *
 * @author Emre Sokullu
 */
class ImpossibleContaineeException extends \Exception {

  public function __construct(AbstractGraph $container, AbstractNode $containee) {
    parent::__construct();
    $this->message = sprintf("A Graph can only contain another Graph. The Graph \"%s\" attempted to contain a \"%s\" with id \"%s\".", $container->getId(), $containee->getLabel(), $containee->getId());
  }

}
