<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts\Exceptions;

use Pho\Kernel\Nodes\Abstracts\NodeInterface;

/**
 * Thrown when an Actor attempts to draft an Object of type that does not exist.
 *
 * @author Emre Sokullu
 */
class ImpossibleDraftTypeException extends \Exception {

  public function __construct(string $actor_id, string $object_label) {
    parent::__construct();
    $this->message = sprintf("The Actor %s attempted to draft a \"%s\" which is not an Object.", $actor_id, $object_label);
  }

}
