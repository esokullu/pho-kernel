<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts\Exceptions;

use Pho\Kernel\Nodes\Abstracts\NodeInterface;

/**
 * Thrown when an Object has no author or it is not transmitted.
 *
 * @author Emre Sokullu
 */
class OrphanObjectException extends \Exception {

  public function __construct(string $id) {
    parent::__construct();
    $this->message = sprintf("The object %s has no author or it is not transmitted.", $id);
  }

}
