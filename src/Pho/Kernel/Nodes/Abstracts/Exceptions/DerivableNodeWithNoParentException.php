<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Abstracts\Exceptions;

use Pho\Kernel\Nodes\Abstracts\NodeInterface;

/**
 * Thrown when a node of invalid or unsupported type
 * is passed as an argument to a function
 *
 * @author Emre Sokullu
 */
class DerivableNodeWithNoParentException extends \Exception {

  public function __construct(NodeInterface $node) {
    $this->message = sprintf("The derivable node %s with the id %s has no parent set.", $node->getLabel(), $node->getId());
  }

}
