<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Exceptions;

/**
 * Thrown attempted to create a new node, but tihs node already exists.
 * Occurs with the root node, because the id is static.
 *
 * @author Emre Sokullu
 */
class NodeExistsException extends \Exception {

}
