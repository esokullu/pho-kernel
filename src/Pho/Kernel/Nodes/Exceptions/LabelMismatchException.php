<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Exceptions;

/**
 * Thrown when the object label and the database labels don't match.
 * This may occur while accessing an existing object.
 *
 * @author Emre Sokullu
 */
class LabelMismatchException extends \Exception {

}
