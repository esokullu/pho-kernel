<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Exceptions;

/**
 * Thrown when given the uuid does not match a User node or does not exist at all.
 *
 * @author Emre Sokullu
 */
class UserNotFoundException extends \Exception {
    function __construct(string $user_id) {
        parent::__construct(
            sprintf("No user with the id: %s", $user_id)
        );
    }
}
