<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Exceptions;

/**
 * Thrown when a node of invalid or unsupported type
 * is passed as an argument to a function 
 *
 * @author Emre Sokullu
 */
class IncompatibleTypeException extends \Exception {

}
