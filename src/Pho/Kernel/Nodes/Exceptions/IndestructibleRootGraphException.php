<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Exceptions;

/**
 * Thrown with an attempt to destroy the root graph.
 *
 * @author Emre Sokullu
 */
class IndestructibleRootGraphException extends \Exception {
  function __construct(): void
  {
      parent::__construct("Can't destroy the root graph.");
  }
}
