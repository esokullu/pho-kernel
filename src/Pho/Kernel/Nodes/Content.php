<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes;

use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\Abstracts\{AbstractObject, NodeInterface};
use Pho\Kernel\Predicates\CorePredicates;

/**
 * Content is an Object that transmits to Graphs (either a final one like
 * Network or subclasses such as Groups.)
 *
 * @author Emre Sokullu
 */
class Content extends AbstractObject implements ContentInterface {

  public function __construct(string $creator_id, string $content = "") {
    parent::__construct();
    $this->setCreator($creator_id)->write($content);
    $this->acl()->chmod(017750); // friends can read and react, others can't even read
    $this->acl()->umask(00023); // he can't let his friends write, and others react.
  }

  /**
   * {@inheritdoc}
   */
  public function write(string $content): void
  {
    $this->setAttribute("content", $content);
    $this->setAttribute("last_update", time());
  }

  /**
   * {@inheritdoc}
   */
  public function read(): string
  {
    return $this->getAttribute("content");
  }


  /**
   * {@inheritdoc}
   */
  public function post(): AbstractObject
  {
    parent::post();
    Kernel::events()->emit("content.posted", [$this->getAuthor()->getId(), $this->getId()]);
    return $this;
  }

}
