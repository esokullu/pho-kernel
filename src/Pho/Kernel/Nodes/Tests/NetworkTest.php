<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Tests;

use Pho\Kernel\Tests\TestCase;
use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\User;

class NetworkTest extends TestCase {

    public function testNewUser() {
        $full_name = "Emre Sokullu";
        $user = $this->network->signUp($full_name);
        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals($full_name, $user->getFullName());

        $user_again = $this->network->logIn($user->getId());
        $this->assertEquals($user->getId(), $user_again->getId());
    }

    

}