<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Tests;

use Pho\Kernel\Tests\TestCase;
use Pho\Kernel\Nodes\NodeFactory;
use Pho\Kernel\Kernel;

class UserTest extends TestCase {

  private $user1, $user2;

  public function setUp() {
    parent::setUp();
    $this->user1 = NodeFactory::build("user");
    $this->user2 = NodeFactory::build("user");
  }

  public function tearDown() {
    unset($this->user1);
    unset($this->user2);
    parent::tearDown();
  }

  public function testFollow() {
    $this->assertFalse($this->user1->isFollowing($this->user2));
    $this->assertFalse($this->user2->isFollowedBy($this->user1));
    $this->user1->follow($this->user2);
    $this->assertTrue($this->user1->isFollowing($this->user2));
    $this->assertTrue($this->user2->isFollowedBy($this->user1));
    $this->assertFalse($this->user2->isFollowing($this->user1));
    $this->assertFalse($this->user1->isFollowedBy($this->user2));
    $this->user2->follow($this->user1);
    $this->assertTrue($this->user2->isFollowing($this->user1));
    $this->assertTrue($this->user1->isFollowedBy($this->user2));
  }
/*
  public function testPosts() {
    $content = NodeFactory::build("content",);
    $orig_content_id = $content->getId();
    Kernel::events()->on("content.posted",
      function($author_id, $content_id) use ($orig_content_id) {
        $this->assertEquals($orig_content_id, $content_id);
        //$this->assertEquals($this->user1->getId(), $author_id);
    });
    $this->user1->postTo($content, $this->network);
    $posts = $this->user1->getPosts();
    $post_ids = [];
    array_walk($posts, function($item, $key) use (&$post_ids) {
      $post_ids[] = $item->getId();
    });
    $this->assertContains($orig_content_id, $post_ids, print_r($post_ids, true));
    $this->assertTrue($this->user1->didPublish($content));
    $this->assertFalse($this->user2->didPublish($content));
  }
*/
  public function testGroups() {
    $group1 = $this->user1->createGroup("Test", "This is a test group");
    $user1_groups = $this->user1->getCreatedGroups();
    $user1_group_ids = [];
    array_walk($user1_groups, function($item, $key) use (&$user1_group_ids) {
      $user1_group_ids[] = $item->getId();
    });
    $this->assertContains($group1->getId(), $user1_group_ids);
    $this->user2->join($group1);
    $user2_groups = $this->user2->getGroups();
    $user2_group_ids = [];
    array_walk($user2_groups, function($item, $key) use (&$user2_group_ids) {
      $user2_group_ids[] = $item->getId();
    });
    $this->assertContains($group1->getId(), $user2_group_ids);
    $this->assertTrue($this->user2->isMember($group1));
  }

  public function testConsume() {
    $content = NodeFactory::build("content", "some content...");
    $orig_content_id = $content->getId();
    $this->assertEquals($orig_content_id, $this->user1->consume($orig_content_id)->getId());
  }

  public function testFullName() {
    $full_name = "Emre Sokullu";
    $this->user2->setFullName($full_name);
    $this->assertEquals($full_name, $this->user2->getFullName());

  }


  public function testDefaultContext() {
    $expected = $this->kernel->config()->root_graph_key;
    $user1 = $this->network->signUp("Emre Sokullu");
    $this->assertEquals($expected, $user1->context()->getId());
  }

  public function testSwitchContext() {
    $user1 = $this->network->signUp("Emre Sokullu");
    $group1 = $user1->createGroup("Pho Developers", "A place for Pho developers to discuss.");
    $user1->switchContext($group1);
    $this->assertEquals($group1->getId(), $user1->context()->getId());
  }

  public function testGetCreator() {
    $user1 = $this->network->signUp("Emre Sokullu");
    $group1 = $user1->createGroup("Pho Developers", "A place for Pho developers to discuss.");
    $this->assertNotNull($group1->getCreator());
    $this->assertEquals($user1->getId(), $group1->getCreator()->getId());
  }


}
