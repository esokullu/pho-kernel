<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Tests;

use Pho\Kernel\Kernel;
use Pho\Kernel\Tests\TestCase;
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Predicates\CorePredicates;
use Pho\Kernel\Nodes\NodeFactory;
use Pho\Kernel\Nodes\{Group, Network};
use Pho\Kernel\Nodes\Abstracts\{AbstractGraph};

class NodeFactoryCreateTest extends TestCase {
  private $node_id;

  public function setUp() {
    parent::setUp();

    $this->group1 = new Group();
    $this->group2 = new Group();
    $this->edges = EdgeFactory::build($this->group1, $this->group2, CorePredicates::CONTAINS);

    $this->setupRedis();

  }

  public function testRoot() {
    $this->network = $this->kernel->graph();
    $this->assertInstanceOf(AbstractGraph::class, $this->network);
  }

  public function testCreateGroup() {
    $group = new Group();
    $this->assertInstanceOf(Group::class, $group);
    $this->node_id = $group->getId();
  }

  public function testReconstruct()  {
    $group = new Group();
    $node_id = $group->getId();
    $node = NodeFactory::reconstruct($node_id);
    $this->assertInstanceOf(Group::class, $node);
  }



}
