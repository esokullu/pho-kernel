<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Tests;

use Pho\Kernel\Tests\TestCase;
use Pho\Kernel\Nodes\NodeFactory;
use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\Content;

class ContentTest extends TestCase {

  private $content1;

  public function setUp() {
    parent::setUp();
    $this->content1 = NodeFactory::build("content", "this is so exciting");
  }

/*
  public function testAuthor() {
    $user1 = NodeFactory::build("user");
    $user1->postTo($this->content1, $this->network);
    $this->assertEquals($user1->getId(), $this->content1->getAuthor()->getId());
  }
*/
  public function testContent() {
    $text_content = "This is lorem ipsum lorem ipsum.";
    $this->content1->write($text_content);
    $this->assertEquals($text_content, $this->content1->read());
  }

  public function testActorModelContentCreation() {
    $user1 = NodeFactory::build("user");
    $content_txt =  "really?";
    $content_obj = $user1->draft("content", "really?")->inContext($this->network)->post();
    $this->assertEquals($content_txt, $content_obj->read());
    $this->assertInstanceOf(Content::class, ($content_obj_clone = NodeFactory::reconstruct($content_obj->getId())));
    $this->assertEquals($content_txt, $content_obj_clone->read());
  }

}
