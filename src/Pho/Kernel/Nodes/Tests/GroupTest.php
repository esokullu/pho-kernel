<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Nodes\Tests;

use Pho\Kernel\Tests\TestCase;
use Pho\Kernel\Nodes\NodeFactory;
use Pho\Kernel\Kernel;

class GroupTest extends TestCase {

  private $group1;

  public function setUp() {
    parent::setUp();
    $this->group1 = NodeFactory::build("group");
  }

  public function tearDown() {
    unset($this->group1);
    parent::tearDown();
  }

  public function testSetters() {
    $title = "My Title";
    $description = "Some description. This can be fairly long.";
    $this->group1->setTitle($title);
    $this->group1->setDescription($description);
    $this->assertEquals($title, $this->group1->getAttribute("title"));
    $this->assertEquals($description, $this->group1->getAttribute("description"));
  }

  public function testLogo() {
    $starbucks_logo = __DIR__ . DIRECTORY_SEPARATOR . "assets" . DIRECTORY_SEPARATOR . "starbucks.jpg";
    $expected_name = $this->group1->getId()."-logo". image_type_to_extension(exif_imagetype($starbucks_logo));
    if(@isset($this->configs["services"]["storage"]["uri"])) {
      $expected_fs =  $this->configs["services"]["storage"]["uri"] . DIRECTORY_SEPARATOR . $expected_name;
    }
    else {
      $expected_fs =  $this->kernel->config()->tmp_path . DIRECTORY_SEPARATOR . $expected_name;
    }

    $this->group1->setLogo($starbucks_logo);
    $this->assertTrue($this->kernel->service("storage")->file_exists($expected_name));
    $this->assertFileExists($expected_fs);
    $this->assertFileEquals($expected_fs, $starbucks_logo);
  }

}
