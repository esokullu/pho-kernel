<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Database;


/**
 * Database acts as a persistent storage for the nodes, their attributes
 * and edges. It's a keeper of truth only, and doesn't help in searching
 * through complex queries.
 */
interface DatabaseInterface {

  /**
   * Stores a value with the given key.
   *
   * @param string $key
   * @param array|string|null $value
   */
  public function set(string $key, $value): void;

  /**
   * Returns the value of the given key or null if there's no
   * such key.
   *
   * @param string $key
   *
   * @return array|string|null
   */
  public function get(string $key);

  /**
   * Deletes a row
   *
   * @param string $key
   */
  public function delete(string $key): void;


  /**
   * Creates a list in the database and adds the value in it. It is like
   * an array in the database row. In redis terms, this is sadd, and it's
   * fast as an O(1) operation.
   *
   * @param string  $list List key.
   * @param string  $value
   */
  public function list_add(string $list, string $value): void;

  /**
   * Fetches the members of the list in the database. In redis terms, this is
   * smembers, and it's fast for small lists as an O(N) operation.
   *
   * @param string  $list List key.
   *
   * @return array The list members.
   */
  public function list_members(string $list): array;

  /**
   * Removes an element from a list in the database. In redis terms, this is
   * srem. Time complexity: O(N) where N is the number of members to be removed.
   *
   * @param string  $list List key.
   * @param string  $value
   */
  public function list_delete(string $list, string $value): void;


  /**
   * Purges the given database entry from the database in given seconds.
   *
   * @param string  $key
   * @param int $timeout Timeout in seconds.
   */
  public function expire(string $key, int $timeout): void;

  /**
   * Returns the remaining timeout for the expire function in seconds. If no
   * expiration was set, returns -1.
   *
   * @param string  $key
   *
   * @return int  Remaining timeout in seconds or -1 if expiration was not set.
   */
  public function getTTL(string $key): int;

}
