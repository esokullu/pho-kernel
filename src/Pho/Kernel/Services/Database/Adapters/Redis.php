<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Database\Adapters;

use Pho\Kernel\Kernel;
use Pho\Kernel\Exceptions\{KernelNotRunningException};
use Pho\Kernel\Services\ServiceInterface;
use Pho\Kernel\Services\Database\DatabaseInterface;
use Pho\Kernel\Services\Exceptions\MissingAdapterExtensionException;
use Predis\Client as RedisClient;

/**
 * Redis adapter as a database.
 *
 * This is the default database of Pho. Works with Predis
 * (https://github.com/nrk/predis)
 *
 * In production, make sure you have the PHP PECL extension
 * phpiredis (https://github.com/nrk/phpiredis) installed for
 * faster performance.
 *
 * @author Emre Sokullu
 */
class Redis implements DatabaseInterface, ServiceInterface {

  /**
   * @var Predis\Client
   */
  private $client;

  public function __construct(string $uri=null) {
    $this->client = new RedisClient($uri);
  }

  public function set(string $key, $value): void
  {
    $this->client->set($key, $value);
  }

  public function get(string $key)
  {
    // returns null automatically if the given key does not exist.
    return $this->client->get($key);
  }

  /**
   * {@inheritdoc}
   */
  public function delete(string $key): void
  {
    $this->client->del($key);
  }

  /**
   * {@inheritdoc}
   */
  public function list_members(string $list): array
  {
    return (array) $this->client->smembers($list);
  }

  /**
   * {@inheritdoc}
   */
  public function list_add(string $list, string $value): void
  {
    $this->client->sadd($list, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function list_delete(string $list, string $value): void
  {
    $this->client->srem($list, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function expire(string $key, int $timeout): void
  {
    Kernel::service("logger")->info(
      sprintf("Expiring %s in %s", $key, (string) $timeout)
    );
    $this->client->expire($key, $timeout);
  }

  /**
   * {@inheritdoc}
   */
  public function getTTL(string $key): int
  {
    return $this->client->ttl($key);
  }

}
