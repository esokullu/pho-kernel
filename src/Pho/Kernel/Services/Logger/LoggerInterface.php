<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Logger;

interface LoggerInterface {

  /**
   * * Logs warning or more critical messages. This is same as warning function.
   *
   * @param string  $message  Log message
   */
  public function log(string $message): void;

  /**
   * Logs warning or more critical messages.
   *
   * @param string  $message  Log message
   */
  public function warning(string $message): void;

  /**
   * Logs informational or debug messages.
   *
   * @param string  $message  Log message
   */
  public function info(string $message): void;
}
