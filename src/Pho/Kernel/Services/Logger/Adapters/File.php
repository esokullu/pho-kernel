<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Logger\Adapters;

use Pho\Kernel\Services\ServiceInterface;
use Pho\Kernel\Services\Logger\LoggerInterface;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * File based logging. The log files specified at the kernel
 * constructor level or via the *configure* function of the kernel.
 *
 * @author Emre Sokullu
 */
class File implements LoggerInterface, ServiceInterface {
  /**
   * @var Monolog\Logger
   */
  private $channel;

  /**
   * Constructor.
   *
   * @param string $uri  the path to log file in uri form.
   */
  public function __construct(string $uri) {
    $this->channel = new Logger('default');
    $this->channel->pushHandler(new StreamHandler($uri, Logger::INFO));
  }

  /**
   * {@inheritdoc}
   */
  public function log(string $message): void
  {
    $this->warning($message);
  }

  /**
   * {@inheritdoc}
   */
  public function warning(string $message): void
  {
    $this->channel->warning($message);
  }

  /**
   * {@inheritdoc}
   */
  public function info(string $message): void
  {
    $this->channel->info($message);
  }

}
