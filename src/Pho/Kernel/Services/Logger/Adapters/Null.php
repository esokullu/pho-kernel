<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Logger\Adapters;

use Pho\Kernel\Services\Logger\LoggerInterface;
use Pho\Kernel\Services\ServiceInterface;

/**
 * You use this class when you do not want to log anything.
 * It's like logging to /dev/null
 *
 * @author Emre Sokullu
 */
class Null implements LoggerInterface, ServiceInterface {

  /**
   * Constructor.
   */
  public function __construct() {}

  /**
   * {@inheritdoc}
   */
  public function warning(string $message): void {}

  /**
   * {@inheritdoc}
   */
  public function info(string $message): void {}

  /**
   * {@inheritdoc}
   */
  public function log(string $message): void {}

}
