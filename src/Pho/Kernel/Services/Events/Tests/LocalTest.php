<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Events\Tests;

use Pho\Kernel\Services\Events\Adapters\Local as LocalEvents;
use Pho\Kernel\Services\Events\Exceptions\InvalidEventException;

class LocalTest extends \PHPUnit_Framework_TestCase {

  private $events;
  private $expected_string_1="phonetworks";
  private $expected_string_2="emre sokullu";

  public function setUp() {
    $this->events = new LocalEvents();
    $this->events->on("test.event", function($var1, $var2) {
        $this->assertEquals($this->expected_string_1, $var1);
        $this->assertEquals($this->expected_string_2, $var2);
    });
  }

  public function tearDown() {
    unset($this->events);
  }

  public function testListen() {
    $this->events->emit("test.event", [$this->expected_string_1, $this->expected_string_2]);
  }

  public function testInvalidEvent() {
    $this->expectException(InvalidEventException::class);
    $this->events->emit("invalid.event");
  }

}
