<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Index\Adapters;

use Pho\Kernel\Services\ServiceInterface;
use Pho\Kernel\Services\Index\IndexInterface;
use Pho\Kernel\Nodes\Abstracts\NodeInterface;

class Mysql implements IndexInterface, ServiceInterface {

  public function index(NodeInterface $node, string $attribute_key, /* mixed */ $attribute_value): void
  {}

  public function createIndex(string $node_class, string $attribute_key): void {}

  public function search(string $node_class, string $attribute_key, string $match): array
  {}

}
