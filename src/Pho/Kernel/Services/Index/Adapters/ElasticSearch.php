<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Index\Adapters;

use Elasticsearch\ClientBuilder;
use Pho\Kernel\Services\Index\IndexInterface;
use Pho\Kernel\Services\ServiceInterface;
use Pho\Kernel\Nodes\Abstracts\NodeInterface;

class ElasticSearch implements IndexInterface, ServiceInterface {

  private $client;

  public function __construct(string $uri=null)
  {

    /******************************************************************************************

    // https://www.elastic.co/guide/en/elasticsearch/client/php-api/5.0/_configuration.html
    $hosts = [
        // This is effectively equal to: "https://username:password!#$?*abc@foo.com:9200/"
        [
            'host' => 'localhost',
            'port' => '9200',
            'scheme' => 'http',
            'user' => 'elastic',
            'pass' => 'changeme'
        ]
    ];
    $client = ClientBuilder::create()->setHosts($hosts)->build();

    *******************************************************************************************/
    $this->client = ClientBuilder::create()->setHosts($uri)->build();
  }

  /**
   * {@inheritdoc}
   */
  public function createIndex(string $node_class, string $attribute_key): void
  {

  }

  /**
   * {@inheritdoc}
   */
  public function index(NodeInterface $node, string $attribute_key, /* mixed */ $attribute_value): void
  {
    $this->client->index([
      "index" => $node::CLASSNAME,
      "type" => "my_type",
      "id" => uniqid(),
      "body" => [ $attribute_key => $attribute_value]
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function search(string $node_class, string $attribute_key, string $match): array
  {
    $this->client->search([
      "index" => $node_class,
      "type" => "my_type",
      'body' => [
        'query' => [
            'match' => [
                $attribute_key => $match
            ]
        ]
      ]
    ]);
  }

}
