<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Index;

use Pho\Kernel\Nodes\Abstracts\NodeInterface;

interface IndexInterface {

  /**
   * Creates a new index for given node and the key.
   *
   * @param string  $node_class
   * @param string  $attribute_key
   */
  public function createIndex(string $node_class, string $attribute_key): void;

  /**
   * Indexes given attribute key/value pair in a designated index.
   *
   * @param NodeInterface  $node
   * @param string  $node_class
   * @param string  $attribute_key
   */
  public function index(NodeInterface $node, string $attribute_key, /* mixed */ $attribute_value): void;

  /**
   * Performs a simple full-text search on given node and key against the match
   * string.
   *
   * @param NodeInterface  $node
   * @param string  $node_class
   * @param string  $match
   *
   * @return  array   An array of search hits in the following format:
   *                    [hits] => Array
   *              (
   *                [0] => Array
   *                      (
   *                          [_index] => my_index
   *                          [_type] => my_type
   *                          [_id] => 58d2b4b654cd7
   *                          [_score] => 0.2876821
   *                          [_source] => Array
   *                              (
   *                                  [testField] => abc
   *                              )
   *
   *                      )
   *
   *                  [1] => Array
   *                      (
   *                          [_index] => my_index
   *                          [_type] => my_type
   *                          [_id] => 58d2b50d6a013
   *                          [_score] => 0.2876821
   *                          [_source] => Array
   *                              (
   *                                    [testField] => abc
   *                              )
   *                        )
   *                )
   *
   */
  public function search(string $node_class, string $attribute_key, string $match): array;
}
