<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Storage\Adapters;

use Pho\Kernel\Kernel;
use Pho\Kernel\Services\ServiceInterface;
use Pho\Kernel\Services\Storage\StorageInterface;
use Pho\Kernel\Services\Storage\Exceptions\InaccessiblePathException;

/**
 * Filesystem Adapter for Storage
 *
 * Storage is a service for long binary persistence and retrieval. It is not
 * used for text, short form of content or keeper of truth.
 *
 * Filesystem is a simple adapter suitable to to single-machine installations,
 * therefore it is not scalable. Refer to OpenStack Swift as a distributed
 * scalable and highly available storage adapter.
 *
 * @author Emre Sokullu
 */
class Filesystem extends FilesystemUtils implements StorageInterface, ServiceInterface {

  private $root;

  public function __construct(?string $uri) {
    if(is_null($uri)||empty($uri)) {
      $this->root = Kernel::config()->tmp_path . DIRECTORY_SEPARATOR . "pho";
    }
    else {
      $this->root = $uri;
    }
    if(!file_exists($this->root)&&!mkdir($this->root)) {
      throw new InaccessiblePathException($this->root);
    }
    Kernel::service("logger")->info(sprintf("The storage service has started with the %s adapter on %s.", __CLASS__, $this->root));
  }

    public function get(string $path): string
    {
      return $this->path_normalize($this->root."/".$path);
    }

    public function mkdir(string $dir, bool $recursive=true): void
    {
      if(!mkdir($this->get($dir), 777, $recursive))
        throw new InaccessiblePathException($dir);
    }

    public function file_exists(string $path): bool
    {
      return file_exists($this->get($path));
    }

  public function put(string $file, string $path): void
  {
    file_put_contents($this->get($path), file_get_contents($file), LOCK_EX);
  }

  public function append(string $file, string $path): void
  {
    file_put_contents($this->get($path), file_get_contents($file), LOCK_EX|FILE_APPEND);
  }

}
