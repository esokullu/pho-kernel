<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Storage\Adapters;

class FilesystemUtils {

  protected function path_normalize(string $path): string
  {
    return str_replace(["/","\\"], DIRECTORY_SEPARATOR, $path);
  }

}
