<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Services\Storage;

interface StorageInterface {

  public function put(string $file, string $path): void;
  public function append(string $file, string $path): void;
  public function get(string $path): string;
  public function mkdir(string $dir, bool $recursive): void;
  public function file_exists(string $path): bool;
}
