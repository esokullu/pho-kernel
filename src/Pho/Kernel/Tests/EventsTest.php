<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Tests;

use Pho\Kernel\Kernel;
use Pho\Kernel\Nodes\{Group, NodeFactory};

class EventsTest extends TestCase {

  public function testListen() {
    $this->kernel->service("events")->on("node.created", function($id, $label) {
      $this->assertEquals("group", $label);
    });
    NodeFactory::build("group");
  }

  public function testHandleNodeCreated() {
    $group = new Group();
    $network = $this->kernel->graph();

    $network_children = $network->getContaineeIds();
    $this->assertContains($group->getId(), $network_children);
  }

}
