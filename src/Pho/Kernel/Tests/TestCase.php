<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Tests;

use Pho\Kernel\Kernel;
use Pho\Kernel\Edges\EdgeFactory;
use Pho\Kernel\Nodes\NodeFactory;

class TestCase extends \PHPUnit\Framework\TestCase
{
    protected $kernel, $redis, $configs, $network;

    /**
     * @var array
     */
    private $log_db_entries = [];

    protected function getKernelConfig()
    {
        return array(
          "services"=>array(
            "database" => array(
              'type' => getenv('DATABASE_TYPE'),
              'uri' => getenv('DATABASE_URI'),
              ),
            "storage" => array(
              "type" => getenv("STORAGE_TYPE"),
              "uri" => getenv("STORAGE_URI"),
              )
          )
        );
    }

    protected function startKernel(): void
    {
      $this->configs = $this->getKernelConfig();
      $this->kernel = new Kernel($this->configs);
      $this->kernel->boot();
      $this->network = $this->kernel->graph();
    }

    protected function stopKernel(): void
    {
      unset($this->redis);
      unset($this->configs);
      unset($this->network);
      $this->kernel->halt();
      unset($this->kernel);
    }

    protected function setupRedis(): void
    {
      $config = $this->getKernelConfig();
      if($config["services"]["database"]["type"]=="redis") {
        $this->redis  = new \Predis\Client($config["services"]["database"]["uri"]);
      }
    }

    protected function startListeners(): void
    {
      $logs=&$this->log_db_entries;
      $this->kernel->events()->on("node.created", function($node_id, $node_label) use (&$logs) {
        $logs[] = ["type"=>"node", "id"=>$node_id];
      });
      $this->kernel->events()->on("edge.created", function($edge, $source, $destination, $predicate) use (&$log) {
        $logs[] = ["type"=>"edge", "id"=>$edge];
      });
    }

    protected function processListeners(): void
    {
      foreach($this->log_db_entries as $log) {
        $entry = null;
        switch($log["type"]) {
          case "edge":
            $entry = EdgeFactory::reconstruct($log["id"]);
          case "node":
            $entry = NodeFactory::reconstruct($log["id"]);
        }
        $entry->destroy();
      }
    }

    public function setUp() {
      $this->startKernel();
      $this->startListeners();
    }

    public function tearDown() {
      $this->processListeners();
      $this->stopKernel();
    }

}
