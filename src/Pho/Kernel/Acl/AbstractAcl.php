<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Acl;

abstract class AbstractAcl implements AclInterface {

    /**
     * Node Creator. Must point to an Actor.
     *
     * @var string
     */
    protected $OWNER;

    /**
     * The context in which the node was created. Must point to a Graph.
     *
     * @var string
     */
    protected $CONTEXT;

    /**
     * Extra groups that this node is assigned to. 
     *
     * @var array <string>
     */
    protected $GROUPS = [];


    /**
     * Node mod in octal format, representing:
     * Sticky Bit * Owner (o) * Group (g) * Subscribers (s)  * Context (c)
     * Inspired by UNIX filesystem representation.
     *
     * @var int
     */
    protected $MOD = 017754;

    /**
     * Node umask in octal format. Inspired by UNIX filesystems,
     * umask ensures the Actor chmods the node in regards to 
     * certain rules. For example, a umask of 02 means context members
     * are not allowed to have write (2) permissions. A umask of "023"
     * means context members can never write or execute (3) and 
     * subscribers cannot write (2).
     *
     * @var int
     */
    protected $UMASK = 000000;

/*
    public function __construct(AbstractActor $owner, ?AbstractGraph $context=null) {
        $this->chown($owner);
        $this->chctx($context);
    }
*/

    public function chmod(int $mod): void {
        $this->MOD = $mod;
        /*
        switch($actor_role) {
            case "o":
                return $this->MOD & 004000;
            case "g":
                return $this->MOD & 000400;
            case "s":
                return $this->MOD & 000040;
            case "s":
            default:
                return $this->MOD & 000004;
        }
        */
    }

    public function chown(AbstractActor $actor): void {
        $this->OWNER = $actor->getId();
    }

    public function chctx(?AbstractGraph $graph = null): void {
        $this->OWNER = $actor->getId();
    }

    public function chgrp(string $context_id): void {}

    // https://www.cyberciti.biz/tips/understanding-linux-unix-umask-value-usage.html
    public function umask(int $mask): void {
        $this->UMASK = $mask;
        // if o or g with no sticky bit or root
    }

    public function isReadable() {
        switch($actor_role) {
            case "root":
                return true;
            case "o":
                return $this->MOD & 004000;
            case "g":
                return $this->MOD & 000400;
            case "s":
                return $this->MOD & 000040;
            case "s":
            default:
                return $this->MOD & 000004;
        }
    }

    public function isWriteable() {
        switch($actor_role) {
            case "root":
                return true;
            case "o":
                return $this->MOD & 002000;
            case "g":
                return $this->MOD & 000200;
            case "s":
                return $this->MOD & 000020;
            case "s":
            default:
                return $this->MOD & 000002;
        }
    }

    public function isExecutable() {
        switch($actor_role) {
            case "root":
                return true;
            case "o":
                return $this->MOD & 001000;
            case "g":
                return $this->MOD & 000100;
            case "s":
                return $this->MOD & 000010;
            case "s":
            default:
                return $this->MOD & 000001;
        }
    }

    public function hasStickyBit() {
        return $this->MOD & 010000;
    }

    protected function getActorRole(string $actor_id) {
        /*
        switch($actor_id) {
            case $this->OWNER:
                return "o";
            case $this->OWNER
        }
*/
        // owner
        // group
        // subscribers
        // context
    }

}