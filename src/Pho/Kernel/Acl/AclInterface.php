<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Acl;

interface AclInterface {
    /**
     * Undocumented function
     *
     * @param int $mod
     * @return void
     */
    public function chmod(int $mod): void;

    public function chown(AbstractActor $actor): void;

    public function chctx(?AbstractGraph $graph): void;

    public function chgrp(string $context_id): void;

    // https://www.cyberciti.biz/tips/understanding-linux-unix-umask-value-usage.html
    public function umask(int $mask): void;
}


/**
 * OWNER: 17
 * GROUPS:
 *  - ROLE:EDITORS
 *  SUBSCRIBERS (for object, owner subscribers, for graph members)
 *  WORLD (CONTEXT): 4 ==> world is context, because context setter may choose it to be read by everyone or context members only, same with groups.
 */

/**
 * rw-r--r--r--T    groups  {editors}  {subscribers}
 * [
 *  OWNER: RWX,
 *  GROUPS: RWX,
 *  SUBSCRIBERS: R-X,
 *  CONTEXT:
 *  WORLD: R--
 * ]
 */

