<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel\Acl;

use Pho\Kernel\Nodes\Abstracts\AbstractNode;
use Pho\Kernel\Edges\AbstractEdge;
use Pho\Kernel\Predicates\{PredicateUtils, CorePredicates};

class AclBroker {

    /**
     * Dependency injection.
     *
     * @var PredicateUtils
     */
    private $predicate_utils;

    public function __construct(PredicateUtils $predicate_utils) {
        $this->predicate_utils = $predicate_utils;
    }


    public function isPermissible(AbstractNode $subject, AbstractNode $object, string $predicate): bool
    {
        // Contains or Belongs --> must have Graph in one side
        // Subscribes or Publishes --> must have Actor
        // Creates or Product --> must have Actor
        // Transmits or Consumes --> must have an Object
        switch($predicate) {
            case CorePredicates::CONTAINS:
                return $subject->acl()->isWriteableBy($object->getCreator());
            case CorePredicates::BELONGS:
                return $object->acl()->isWriteableBy($subject->getCreator());
            case CorePredicates::SUBSCRIBES:
                return $object->acl()->isExecutableBy($subject);
            case CorePredicates::PUBLISHES:
                return $subject->acl()->isExecutableBy($object); 
            case CorePredicates::CREATES:
                return $subject->context()->acl()->isWriteableBy($subject);
            case CorePredicates::OBJECT:
                return $object->context()->acl()->isWriteableBy($object);
            case CorePredicates::TRANSMITS:
                if($object instanceof AbstractGraph)
                    return $object->isWritableBy($subject->getCreator());
                // else if($object instanceof AbstractActor) // check for ban?
                else if($object instanceof AbstactObject)
                    return $object->isExecutableBy($subject->getCreator());
            case CorePredicates::CONSUMES:
                if($subject instanceof AbstractGraph)
                    return $subject->isWritableBy($object->getCreator());
                // else if($object instanceof AbstractActor) // check for ban?
                else if($subject instanceof AbstactObject)
                    return $subject->isExecutableBy($object->getCreator());
        }

        
    }

}