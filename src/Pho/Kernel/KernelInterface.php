<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Pho\Kernel;

use Pho\Kernel\Services\ServiceInterface;
use Pho\Kernel\Nodes\Abstracts\AbstractGraph;
use Pho\Kernel\Exceptions\{KernelNotRunning, ServiceUnavailable, KernelIsAlreadyRunningException};
use Zend\Config\Config;

interface KernelInterface {

  /**
   * Configuration variables can be passed to the kernel either
   * at construction (e.g. $kernel = new Kernel(...);)
   * or afterwards, using this function $kernel->configure(...);
   * Please note, this function should be run before calling the
   * *boot* function, or otherwise it will not have an effect
   * and throw an exception.
   *
   * @param array $configs Service configurations.
   *
   * @throws KernelIsAlreadyRunningException When run after the kernel has booted up.
   */

  public static function reconfigure( ?array $configs ): void;

  /**
   * Once the configuration is set, run "boot" to start the kernel.
   * Please note, you will not be able to reconfigure the kernel or
   * register new nodes after this point or you will encounter the
   * KernelIsAlreadyRunningException exception.
   *
   * @throws KernelIsAlreadyRunningException When run after the kernel has booted up.
   *
   */
  public function boot(): void;

  /**
   * Frees up the resources and stops the kernel so that it can be reinstated.
   *
   * @throws KernelNotRunningException if the kernel has not booted up yet.
   */
  public function halt(): void;

  /**
   * After the kernel is booted, you can call this function to
   * get the root node in the graph (which is the graph itself)
   * and play with it.
   *
   * @return Pho\Kernel\Nodes\Abstracts\AbstractGraph
   */
  public function graph(): AbstractGraph;

  /**
   * This function returns a kernel service if the kernel
   * has booted up and the service is available. Otherwise,
   * it throws an exception.
   *
   * Available services are:
   *   - config
   *   - database
   *   - index
   *   - logger
   *   - storage
   *   - events
   *
   * @param string  $service_type
   *
   * @return Pho\Kernel\Services\ServiceInterface
   *
   * @throws ServiceUnavailableException if there is not such a service available.
   * @throws KernelNotRunningException if the kernel has not booted up yet.
   */
  public static function service(string $service_type): ServiceInterface;

  /**
   * Provides global access to kernel settings and defaults.
   *
   * @link https://docs.zendframework.com/zend-config/intro/
   *
   * @return Zend\Config\Config The config object.
   */
  public static function config(): Config;

  /**
   * Provides public access to the event broker of the
   * kernel. Also a shortut to the service("events") method.
   * Our event broker is derived from Sabre Events.
   *
   * @link http://sabre.io/event/eventemitter/
   *
   * @return EventEmitter
   */
  public static function events(): ServiceInterface;

  public function registerEntity(): void;

  public function registerAdapter(): void;

  public function listNodes(): array;

  public function index(): void;

  public static function isToIndex(string $node_label, string $attribute): bool;


// not sure if we should enable these.
//    public function listRunningServices(): array;
//    public function isOn(): bool;



}
