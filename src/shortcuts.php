<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

function pk__log(string $message, ...$args):void  
{
    Kernel::service("logger")->info(
        sprintf($message, ...$args)
    );
}


function pk__warn(string $message, ...$args):void 
{
    Kernel::service("logger")->warning(
        sprintf($message, ...$args)
    );
}

function pk__event_emit(string $event_name, callable $callback) {
    Kernel::Service("events")->emit($event_name, $callback);
}