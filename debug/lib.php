<?php

/*
 * This file is part of the Pho package.
 *
 * (c) Emre Sokullu <emre@phonetworks.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/************************************************************
 * This script shows you how to interact with the Pho Kernel
 *
 * @author Emre Sokullu
 ************************************************************/

 // 1. Initiate the autoloaders first.
require(__DIR__."/../vendor/autoload.php");

// 2. Set up the configuration.
$configs = array();
$configs["database"] = [
  "type"=>"redis",
  "uri"=>"tcp://127.0.0.1:6379"
];

// 3. For the sake of this demonstration, let's create a Redis Client that we
// can play with. You won't need to do this otherwise.
$redis = new Predis\Client($configs["database"]["uri"]);

// 4. Time to boot up the pho kernel.
$kernel = new Pho\Kernel\Kernel();
$kernel->reconfigure($configs);
$kernel->boot();

// 5. $network is the main graph.
$network=$kernel->graph();


/********************************************************
 * In this section we will define some general-purpose
 * functions that you may use
 ********************************************************/

/**
 * Lists Redis keys
 *
 * @return void
 */
function ls(): void
{
  global $redis;
  print_r($redis->keys("*"));
}

/**
 * Cleans up the redis database. Use with caution!
 *
 * @return void
 */
function cleanup(): void
{
  global $redis;
  $redis->flushdb();
}

/**
 * Creates a group object.
 *
 * @return Pho\Kernel\Nodes\Group The Group object.
 */
function group(): Pho\Kernel\Nodes\Group
{
  return new Pho\Kernel\Nodes\Group();
  // same as...
  // return Pho\Kernel\Nodes\NodeFactory::build("Group");
}
