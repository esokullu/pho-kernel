<?php
include("lib.php");
$events_service = $kernel->service("events");
$events_list = $events_service->listValidEvents();
foreach($events_list as $event) {
    $events_service->on($event, function(...$args) use ($event) {
        echo $event.PHP_EOL;
        print_r($args);
        echo "--------------------------".PHP_EOL;
    });
}